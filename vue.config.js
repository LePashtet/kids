// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
//   .BundleAnalyzerPlugin;

module.exports = {
  publicPath: '/',
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/assets/fonts/stylesheet.scss";
          @import "@/assets/scss/_variables.scss";
          @import "@/assets/scss/_mixins.scss";
          @import "@/assets/scss/main.scss";
        `,
      },
    },
  },
  chainWebpack: config => {
    config.module
      .rule('fonts')
      .use('url-loader')
      .loader('url-loader')
      .tap(options => Object.assign(options, { limit: 10240 }));
  },
  configureWebpack: {
    // plugins: [new BundleAnalyzerPlugin()],
  },
  pwa: {
    name: 'Justapp',
    appleMobileWebAppCapable: 'yes',
    workboxPluginMode: 'GenerateSW',
    workboxOptions: {
      skipWaiting: true,
      clientsClaim: true,
    },
    themeColor: '#0ca8ad',
  },
};
