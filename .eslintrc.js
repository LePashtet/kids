module.exports = {
  root: true,
  env: {
    node: true,
    es6: true,
    es2017: true,
    es2020: true
  },
  'extends': [
    'plugin:vue/essential',
    "plugin:prettier/recommended",
    'eslint:recommended'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
