//Mixin for select dates
const dayList = [];
for (let i = 1; i < 32; i++) {
  let obj = {};
  obj.name = i;
  obj.value = i;
  dayList.push(obj);
}

const yearList = [];
const childYears = [];
const currentYear = new Date().getFullYear();
for (let i = 75; i >= 1; i--) {
  let obj = {};
  obj.name = currentYear - i;
  obj.value = currentYear - i;
  yearList.unshift(obj);
}
for (let i = 18; i >= 1; i--) {
  let obj = {};
  obj.name = currentYear - i;
  obj.value = currentYear - i;
  childYears.unshift(obj);
}

export const dates = {
  data() {
    return {
      days: dayList,
      monthes: [
        { name: this.$t('constants.january'), value: 1 },
        { name: this.$t('constants.february'), value: 2 },
        { name: this.$t('constants.march'), value: 3 },
        { name: this.$t('constants.april'), value: 4 },
        { name: this.$t('constants.may'), value: 5 },
        { name: this.$t('constants.june'), value: 6 },
        { name: this.$t('constants.july'), value: 7 },
        { name: this.$t('constants.august'), value: 8 },
        { name: this.$t('constants.september'), value: 9 },
        { name: this.$t('constants.october'), value: 10 },
        { name: this.$t('constants.november'), value: 11 },
        { name: this.$t('constants.december'), value: 12 },
      ],
      years: yearList,
      childYears: childYears,
    };
  },
};
