export const notifications = {
  data() {
    return {
      sender: {
        admin: 'Сообщение от Администрации сайта.',
        camp_admin: 'Сообщение от Администрации лагеря.',
      },
    };
  },
};
