export default {
  data() {
    return {
      menuItems: [
        {
          name: 'Рецепты',
          route: '/kitchen',
        },
        {
          name: 'Прием товара',
          route: '/kitchen/acceptance/',
        },
        {
          name: 'Документы приема товара',
          route: '/kitchen/acceptance-list/',
        },
        {
          name: 'Списание',
          route: '/kitchen/write-off/',
        },
        {
          name: 'Документы списания товара',
          route: '/kitchen/write-off-list/',
        },
      ],
    };
  },
};
