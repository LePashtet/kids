import { i18n } from '@/lang/i18n';

export const mainLinks = {
  data() {
    return {
      headerMenuItems: [
        {
          name: i18n.t('home.header.about'),
          route: '/about',
        },
        {
          name: i18n.t('home.header.security'),
          route: '/security',
        },
        {
          name: i18n.t('home.header.partners'),
          route: '/partner',
        },
        {
          name: i18n.t('home.header.rates'),
          route: '/rates',
        },
        {
          name: i18n.t('home.header.contacts'),
          route: '/contacts',
        },
      ],
      footerMenuItems: [
        {
          name: i18n.t('home.header.about'),
          route: '/about',
        },
        {
          name: i18n.t('home.header.security'),
          route: '/security',
        },
        {
          name: i18n.t('home.header.partners'),
          route: '/partner',
        },
        {
          name: i18n.t('home.header.contacts'),
          route: '/contacts',
        },
      ],
      footerSubMenuItems: [
        {
          name: i18n.t('home.footer.subMenu.news'),
          route: '/#',
          disabled: true,
        },
        {
          name: i18n.t('home.footer.subMenu.brandBook'),
          route: '/brand-center',
          disabled: false,
        },
      ],
    };
  },
};
