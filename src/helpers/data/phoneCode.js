export const phoneCodes = {
  data() {
    return {
      countryCode: [
        {
          value: 7,
          title: '+7',
        },
        {
          value: 1,
          title: '+1',
        },
        {
          value: 31,
          title: '+31',
        },
        {
          value: 38,
          title: '+38',
        },
      ],
    };
  },
};
