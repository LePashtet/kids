import { i18n } from '@/lang/i18n';

export const userFooter = {
  data() {
    return {
      footerSubMenu: [
        {
          name: i18n.t('user.footer.subMenu.news'),
          route: '/#',
          disabled: true,
        },
        {
          name: i18n.t('user.footer.subMenu.brandBook'),
          route: '/brand-center',
          disabled: false,
        },
      ],
    };
  },
};

export const kidsCategoriesLink = {
  data() {
    return {
      ParentKids: [
        {
          id: 'camp',
          name: i18n.t('user.titles.menu.camp'),
          menu: [
            {
              id: 'account',
              icon: () => {
                return require('@/assets/img/svg/user-icons/wallet.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/user-icons/wallet-active.svg');
              },
              name: i18n.t('user.titles.menu.account'),
              route: '/parents/kids-account',
            },
            {
              id: 'store',
              icon: () => {
                return require('@/assets/img/svg/user-icons/cart.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/user-icons/cart-active.svg');
              },
              name: i18n.t('user.titles.menu.onlineStore'),
              route: '/parents/online-store',
            },
            {
              id: 'object',
              icon: () => {
                return require('@/assets/img/svg/user-icons/group.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/user-icons/group-active.svg');
              },
              name: i18n.t('user.titles.menu.object'),
              route: '/parents/kids-object',
            },
            {
              id: 'messages',
              icon: () => {
                return require('@/assets/img/svg/user-icons/support.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/user-icons/support-active.svg');
              },
              name: i18n.t('user.titles.menu.serviceMessages'),
              route: '/parents/service-messages',
            },
            {
              id: 'authority',
              icon: () => {
                return require('@/assets/img/svg/add-user-black.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/add-user-black.svg');
              },
              name: i18n.t('user.titles.application'),
              route: '/parents/authority',
            },
            {
              id: 'documents',
              icon: () => {
                return require('@/assets/img/svg/common-icons/folder.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/common-icons/folder-open.svg');
              },
              name: i18n.t('user.titles.menu.documents'),
              route: '/parents/documents',
            },
            {
              id: 'calendar',
              icon: () => {
                return require('@/assets/img/svg/calendar-black.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/calendar-black.svg');
              },
              name: i18n.t('user.titles.menu.calendar'),
              route: '/parents/kid-calendar',
            },
          ],
        },
        {
          id: 'school',
          name: i18n.t('user.titles.menu.school'),
          menu: [
            {
              id: 'authority',
              icon: () => {
                return require('@/assets/img/svg/add-user-black.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/add-user-black.svg');
              },
              name: i18n.t('user.titles.application'),
              route: '/parents/authority',
            },
            {
              id: 'documents',
              icon: () => {
                return require('@/assets/img/svg/common-icons/folder.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/common-icons/folder-open.svg');
              },
              name: i18n.t('user.titles.menu.documents'),
              route: '/parents/documents',
            },
            {
              id: 'calendar',
              icon: () => {
                return require('@/assets/img/svg/calendar-black.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/calendar-black.svg');
              },
              name: i18n.t('user.titles.menu.calendar'),
              route: '/parents/kid-calendar',
            },
          ],
        },
        {
          id: 'kindergarten',
          name: i18n.t('user.titles.menu.kindergarten'),
          menu: [
            {
              id: 'calendar',
              icon: () => {
                return require('@/assets/img/svg/calendar-black.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/calendar-black.svg');
              },
              name: i18n.t('user.titles.menu.calendar'),
              route: '/parents/kid-calendar',
            },
            {
              id: 'documents',
              icon: () => {
                return require('@/assets/img/svg/common-icons/folder.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/common-icons/folder-open.svg');
              },
              name: i18n.t('user.titles.menu.documents'),
              route: '/parents/documents',
            },
            {
              id: 'account',
              icon: () => {
                return require('@/assets/img/svg/user-icons/wallet.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/user-icons/wallet-active.svg');
              },
              name: i18n.t('user.titles.menu.account'),
              route: '/parents/kids-account',
            },
            {
              id: 'notifications',
              icon: () => {
                return require('@/assets/img/svg/message.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/message.svg');
              },
              name: i18n.t('user.titles.menu.serviceMessages'),
              route: '/parents/service-messages',
            },
            {
              id: 'object',
              icon: () => {
                return require('@/assets/img/svg/user-icons/group.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/user-icons/group-active.svg');
              },
              name: i18n.t('user.titles.menu.object'),
              route: '/parents/kids-object',
            },
          ],
        },
      ],
      Kids: [
        {
          id: 'camp',
          name: i18n.t('user.titles.menu.camp'),
          menu: [
            {
              id: 'account',
              icon: () => {
                return require('@/assets/img/svg/user-icons/wallet.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/user-icons/wallet-active.svg');
              },
              name: i18n.t('user.titles.menu.account'),
              route: '/kids/account',
            },
            {
              id: 'store',
              icon: () => {
                return require('@/assets/img/svg/user-icons/cart.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/user-icons/cart-active.svg');
              },
              name: i18n.t('user.titles.menu.onlineStore'),
              route: '/kids/store',
            },
            {
              id: 'object',
              icon: () => {
                return require('@/assets/img/svg/user-icons/group.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/user-icons/group-active.svg');
              },
              name: i18n.t('user.titles.menu.object'),
              route: '/kids/object',
            },
            {
              id: 'messages',
              icon: () => {
                return require('@/assets/img/svg/user-icons/support.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/user-icons/support-active.svg');
              },
              name: i18n.t('user.titles.menu.serviceMessages'),
              route: '/kids/service-messages',
            },
            {
              id: 'documents',
              icon: () => {
                return require('@/assets/img/svg/common-icons/folder.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/common-icons/folder-open.svg');
              },
              name: i18n.t('user.titles.menu.documents'),
              route: '/kids/documents',
            },
          ],
        },
        {
          id: 'school',
          name: i18n.t('user.titles.menu.school'),
          menu: [],
        },
        {
          id: 'kindergarten',
          name: i18n.t('user.titles.menu.kindergarten'),
          menu: [
            {
              id: 'account',
              icon: () => {
                return require('@/assets/img/svg/user-icons/wallet.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/user-icons/wallet-active.svg');
              },
              name: i18n.t('kinder.account.title'),
              route: '/kids/kinder/account',
            },
            {
              id: 'documents',
              icon: () => {
                return require('@/assets/img/svg/common-icons/folder.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/common-icons/folder-open.svg');
              },
              name: i18n.t('kinder.documents.title'),
              route: '/kids/kinder/documents',
            },
            {
              id: 'notifications',
              icon: () => {
                return require('@/assets/img/svg/message.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/message.svg');
              },
              name: i18n.t('user.titles.menu.serviceMessages'),
              route: '/kids/service-messages',
            },
            {
              id: 'object',
              icon: () => {
                return require('@/assets/img/svg/user-icons/group.svg');
              },
              iconActive: () => {
                return require('@/assets/img/svg/user-icons/group-active.svg');
              },
              name: i18n.t('user.titles.menu.object'),
              route: '/kids/kinder/object',
            },
          ],
        },
      ],
    };
  },
};

export const kidsParentsMenu = {
  data() {
    return {
      menu: [
        {
          id: 'message',
          icon: () => {
            return require('@/assets/img/svg/message.svg');
          },
          name: i18n.t('user.titles.menu.message'),
        },
        {
          id: 'info',
          icon: () => {
            return require('@/assets/img/svg/alert-round.svg');
          },
          name: i18n.t('user.titles.menu.info'),
        },
        {
          id: 'alert',
          icon: () => {
            return require('@/assets/img/svg/alert-triangle.svg');
          },
          name: i18n.t('user.titles.menu.alert'),
        },
      ],
    };
  },
};
