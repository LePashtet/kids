import { required } from 'vuelidate/lib/validators';

export default {
  user: {
    id: {
      required,
    },
  },
};
