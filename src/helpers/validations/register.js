import {
  required,
  email,
  sameAs,
  minLength,
  maxLength,
} from 'vuelidate/lib/validators';

// const mustBeTrue = (value) => value === true;

const loginValidate = {
  auth: {
    login: {
      required,
    },
    password: {
      required,
    },
  },
};

const forgotValidate = {
  forgot: {
    prephone: {
      required,
    },
    phone: {
      required,
    },
    surname: {
      required,
    },
    email: {
      required,
      email,
    },
  },
};

const parrentValidate = {
  register: {
    userYear: {
      required,
    },
    name: {
      required,
    },
    second_name: {
      required,
    },
    email: {
      required,
      email,
    },
    phone: {
      required,
      minLength: minLength(11),
      maxLength: maxLength(17),
    },
    password: {
      required,
    },
    passwordRepeat: {
      required,
      sameAsPassword: sameAs('password'),
    },
    personalData: {
      check: val => val === true,
    },
  },
};

const kidValidate = {
  register: {
    userYear: {
      required,
    },
    email: {
      required,
      email,
    },
    phone: {
      required,
    },
    parentsEmail: {
      required,
      email,
    },
  },
};

const objectValidate = {
  register: {
    name: {
      required,
    },
    fullName: {
      required,
    },
    address: {
      required,
    },
    paymentAccount: {
      required,
      minLength: minLength(20),
      maxLength: maxLength(20),
    },
  },
};

const objectInfoValidate = {
  register: {
    objectEmail: {
      required,
    },
  },
};

export {
  loginValidate,
  forgotValidate,
  parrentValidate,
  kidValidate,
  objectValidate,
  objectInfoValidate,
};
