import { coreRepository, acquiringRepository } from './Repository';

const auth = '/auth';
const camp = '/camp';
const account = 'account';

export default {
  getUserInfo(payload) {
    return coreRepository.get(`${auth}/user`, { params: payload });
  },
  // getAccountLog(payload) {
  //   return coreRepository.get(`${account}/logs`, {
  //     params: { ...payload },
  //   });
  // },
  // getKidAccount(payload) {
  //   return coreRepository.post(`${auth}/account`, payload);
  // },
  // getKidAccountLogs(payload) {
  //   return coreRepository.post(`${auth}/accountlogs`, payload);
  // },
  // getKidBills(payload) {
  //   return coreRepository.post(`${auth}/userchecks`, payload);
  // },
  // getCampInfo(payload) {
  //   return coreRepository.get(`${camp}/getdata`, {
  //     params: { ...payload },
  //   });
  // },
  // getSquadInfo(payload) {
  //   return coreRepository.get(`${camp}/squad`, {
  //     params: { ...payload },
  //   });
  // },
  getAssignedKids(payload) {
    return coreRepository.get(`${auth}/getlinkedchild`, {
      params: { ...payload },
    });
  },
  findKid(payload) {
    return coreRepository.get(`${auth}/findchild`, {
      params: { ...payload },
    });
  },
  findUser(payload) {
    return coreRepository.get(`${auth}/finduser`, {
      params: { ...payload },
    });
  },
  getLinkedParents(payload) {
    return coreRepository.get(`${auth}/getlinkedparent`, {
      params: { ...payload },
    });
  },
  // getAccountInfo(payload) {
  //   return acquiringRepository.post(`${account}/batch-view`, payload);
  // },
  getAccountInfo(payload) {
    return acquiringRepository.get(`${account}/view`, {
      params: {
        ...payload,
      },
    });
  },
  getAccountLogs(payload) {
    return acquiringRepository.get(`account-log/index`, {
      params: {
        ...payload,
      },
    });
  },
  getAccountBills(payload) {
    return acquiringRepository.get(`bill/index`, {
      params: {
        ...payload,
      },
    });
  },
  getAccountBill(payload) {
    return acquiringRepository.get(`bill/view`, {
      params: {
        ...payload,
      },
    });
  },
};
