import { outletRepository } from './Repository';

const outlet = '/onlineoutlet';
const category = '/onlinecategory';
const product = '/onlineproduct';
const payment = '/onlinepayment';

export default {
  getOutlet(payload) {
    return outletRepository.get(`${outlet}/find`, {
      params: { ...payload },
    });
  },
  getCategories(payload) {
    return outletRepository.get(`${category}/gettree`, {
      params: { ...payload },
    });
  },
  findProducts(payload) {
    return outletRepository.get(`${product}/find`, {
      params: { ...payload },
    });
  },
  getProduct(payload) {
    return outletRepository.get(`${product}/get`, {
      params: { ...payload },
    });
  },
  buyProduct(payload) {
    return outletRepository.post(`${payment}/buyproducts`, payload);
  },
  userPurchases(payload) {
    // payload: user_id
    return outletRepository.get(`${payment}/getpurchasesbyuser`, {
      params: { ...payload },
    });
  },
  refundProduct(payload) {
    //payload:
    // "outlet_id": 5,
    // "user_id": 13,
    // "purchase_id": 4
    return outletRepository.post(`${payment}/refundproducts`, payload);
  },
  //
  // addCategory(payload) {
  //     return outletRepository.post(`${category}/createcategory`, payload);
  // },
};
