import { calendarRepository } from './Repository';

export default {
  getEvent(payload) {
    return calendarRepository.get(`/get`, {
      params: { ...payload },
    });
  },
  findEvents(payload) {
    return calendarRepository.get(`/find`, {
      params: { ...payload },
    });
  },

  //
  // addCategory(payload) {
  //     return outletRepository.post(`${category}/createcategory`, payload);
  // },
};
