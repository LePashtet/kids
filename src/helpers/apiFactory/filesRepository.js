import { coreRepository, documentRepository } from './Repository';

const documents = '/documents';

export default {
  getDocumentsTypes() {
    return coreRepository.get(`${documents}/types`);
  },
  uploadFile(payload) {
    return documentRepository.post(`${documents}/upload`, payload);
  },
  getFilesByType(payload) {
    return coreRepository.get(`${documents}/list`, {
      params: {
        ...payload,
      },
    });
  },
  deleteFile(payload) {
    return coreRepository.post(`${documents}/delete`, payload);
  },
};
