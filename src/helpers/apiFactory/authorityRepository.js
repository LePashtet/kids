import { coreRepository, documentRepository } from './Repository';

const authority = '/authority';

export default {
  createAuthority(payload) {
    return coreRepository.post(`${authority}/create-request`, payload);
  },
  getAuthority(payload) {
    return coreRepository.get(`${authority}/view-request`, {
      params: {
        ...payload,
      },
    });
  },
  uploadFile(payload) {
    return documentRepository.post(`${authority}/upload-file`, payload);
  },
  deleteFile(payload) {
    return coreRepository.post(`${authority}/delete-file`, payload);
  },

  getAuthorities(payload) {
    return coreRepository.get(`${authority}/authorities-by-child`, {
      params: { ...payload },
    });
  },
};
