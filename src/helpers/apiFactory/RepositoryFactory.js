import AuthRepository from './authRepository';
import OutletRepository from './outletRepository';
import OnlineOutletRepository from './onlineOutletRepository';
import CommonRepository from '@/helpers/apiFactory/commonRepository';
import NotificationsRepository from '@/helpers/apiFactory/notificationsRepository';
import UserRepository from '@/helpers/apiFactory/userRepository';
import ServiceMessagesRepository from '@/helpers/apiFactory/serviceMessagesRepository';
import CalendarRepository from '@/helpers/apiFactory/calendarRepository';
import attendanceRepository from '@/helpers/apiFactory/attendanceRepository';
import AuthorityRepository from '@/helpers/apiFactory/authorityRepository';
import filesRepository from '@/helpers/apiFactory/filesRepository';

import refreshHeaders from '@/helpers/apiFactory/refreshHeaders';

const repositories = {
  auth: AuthRepository,
  outlet: OutletRepository,
  onlineOutlet: OnlineOutletRepository,
  common: CommonRepository,
  user: UserRepository,
  serviceMessages: ServiceMessagesRepository,
  notifications: NotificationsRepository,
  calendar: CalendarRepository,
  authority: AuthorityRepository,
  files: filesRepository,
  attendance: attendanceRepository,

  refreshHeaders,
};

export const RepositoryFactory = {
  get: name => repositories[name],
};
