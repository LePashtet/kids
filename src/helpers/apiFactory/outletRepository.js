import { outletRepository } from './Repository';

const kitchen = '/offlineoutletkitchen';
const payments = '/offlinepayment';
const outlet = '/offlineoutlet';
const category = '/offlinecategory';
const product = '/offlineproduct';
const provider = '/offlineprovider';
const report = '/offlinereport';

export default {
  addCategory(payload) {
    return outletRepository.post(`${category}/createcategory`, payload);
  },
  updateProduct(payload) {
    return outletRepository.post(`${product}/updateproduct`, payload);
  },
  buyProduct(payload) {
    return outletRepository.post(`${payments}/buyproducts`, payload);
  },
  buyOnlineProduct(payload) {
    return outletRepository.post(`${payments}/buyproductsonline`, payload);
  },
  returnProduct(payload) {
    return outletRepository.post(`${payments}/refundproducts`, payload);
  },
  postAcceptance(payload) {
    return outletRepository.post(`${product}/arriveproducts`, payload);
  },
  postOvercharge(payload) {
    return outletRepository.post(`${product}/repriceproducts`, payload);
  },
  postWriteOff(payload) {
    return outletRepository.post(`${product}/writeoffproducts`, payload);
  },
  postBarCode(payload) {
    return outletRepository.post(`${product}/addbarcodes`, payload);
  },
  removeBaCode(payload) {
    return outletRepository.post(`${product}/deletebarcode`, payload);
  },
  getOverchargeList(payload) {
    return outletRepository.get(`${report}/getrepricingreport`, {
      params: { ...payload },
    });
  },
  getProductsStatuses() {
    return outletRepository.get(`${product}/getproductstatuses`);
  },
  getOutletsList(payload) {
    return outletRepository.get(`${outlet}/getoutlets`, {
      params: { ...payload },
    });
  },
  getUserOutletsList(payload) {
    return outletRepository.get(`${outlet}/getoutletbyuser`, {
      params: { ...payload },
    });
  },
  //////////////////
  // BANS & LIMITS
  //////////////////
  getProductRestricts(payload) {
    return outletRepository.get(`${product}/getbanproducts`, {
      params: { ...payload },
    });
  },
  getCategoriesRestricts(payload) {
    return outletRepository.get(`${category}/getbancategories`, {
      params: { ...payload },
    });
  },
  getCashLimit(payload) {
    return outletRepository.get(`${payments}/getcashlimit`, {
      params: { ...payload },
    });
  },
  setCashLimit(payload) {
    return outletRepository.post(`${payments}/setcashlimit`, payload);
  },
  banCategories(payload) {
    return outletRepository.post(`${category}/bancategories`, payload);
  },
  unBanCategories(payload) {
    return outletRepository.post(`${category}/deletebancategories`, payload);
  },
  banProducts(payload) {
    return outletRepository.post(`${product}/banproducts`, payload);
  },
  unBanProducts(payload) {
    return outletRepository.post(`${product}/deletebanproducts`, payload);
  },
  //////////////////
  //////////////////
  getProvidersList(payload) {
    return outletRepository.get(`${provider}/getproviders`, {
      params: { ...payload },
    });
  },
  getKitchenList(payload) {
    return outletRepository.get(`${kitchen}/getkitchens`, {
      params: { ...payload },
    });
  },
  getKitchenInfo(payload) {
    return outletRepository.get(`${kitchen}/getkitchen`, {
      params: { ...payload },
    });
  },
  getKitchenProducts(payload) {
    return outletRepository.get(`${kitchen}/getkitchenproducts`, {
      params: { ...payload },
    });
  },
  getKitchenCategories(payload) {
    return outletRepository.get(`${kitchen}/getcategories`, {
      params: { ...payload },
    });
  },
  getKitchenRecipes(payload) {
    return outletRepository.get(`${kitchen}/getkitchenrecipes`, {
      params: { ...payload },
    });
  },
  getKitchenAcceptance(payload) {
    return outletRepository.get(`${kitchen}/getarrivalorders`, {
      params: { ...payload },
    });
  },
  getKitchenAcceptanceItem(payload) {
    return outletRepository.get(`${kitchen}/getarrivalorder`, {
      params: { ...payload },
    });
  },
  getKitchenWriteOffs(payload) {
    return outletRepository.get(`${kitchen}/getwriteofforders`, {
      params: { ...payload },
    });
  },
  getKitchenWriteOff(payload) {
    return outletRepository.get(`${kitchen}/getwriteofforder`, {
      params: { ...payload },
    });
  },
  getCurrentShopCategories(payload) {
    return outletRepository.get(`${category}/gettree`, {
      params: { ...payload },
    });
  },
  getCurrentShopProducts(payload) {
    return outletRepository.get(`${product}/findproducts`, {
      params: { ...payload },
    });
  },
};
