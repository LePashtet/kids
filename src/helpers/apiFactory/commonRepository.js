import { coreRepository, commonRepository } from './Repository';

const resource = '/auth';
const counter = '/counter';
export default {
  getCounter() {
    return commonRepository.get(counter, {});
  },

  getUserByAccount(payload) {
    return coreRepository.post(`${resource}/userbyaccount`, payload);
  },
  getDepartmentInfo(payload) {
    return coreRepository.get('/department/view', {
      params: {
        ...payload,
      },
    });
  },
};
