import axios from 'axios';
// import refreshToken from '@/helpers/apiFactory/refreshToken';

const coreDomain = process.env.VUE_APP_API_CORE;
const outletDomain = process.env.VUE_APP_API_OUTLET;
const notificationsDomain = process.env.VUE_APP_API_NOTIFICATIONS;
const acquiringDomain = process.env.VUE_APP_API_ACQUIRING;
const serviceMessagesDomain = process.env.VUE_APP_API_SERVICEMESSAGES;
const calendarDomain = process.env.VUE_APP_API_CALENDAR;
const attendanceDomain = process.env.VUE_APP_API_ATTENDANCE;
const commonDomain = process.env.VUE_APP_API_COMMON;
const coreUrl = `${coreDomain}/rest/v1`;
const outletUrl = `${outletDomain}/rest/v1`;
const notificationsUrl = `${notificationsDomain}/v1/notification`;
const serviceMessagesUrl = `${serviceMessagesDomain}/rest/v1`;
const calendarUrl = `${calendarDomain}/rest/v1/calendar`;
const attendanceUrl = `${attendanceDomain}/rest/v1/`;
const acquiringUrl = `${acquiringDomain}/api`;
const commonUrl = `${commonDomain}`;

const authUrl = `${coreDomain}`;

// async function headers() {
//   return {
//     'Content-Type': 'application/json',
//     Authorization: 'Bearer ' + localStorage.getItem('token'),
//     uid: localStorage.getItem('uid'),
//   };
// }

const authRepository = axios.create({
  baseURL: authUrl,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Source: 'rest-api',
  },
});

// authRepository.interceptors.response
//   .use
// response => {
//   // Return a successful response back to the calling service
//   if (response.data.status == 'ok') {
//     console.log('success interceptor response', response);
//     console.log(
//       'success interceptor response!!!',
//       authRepository.defaults.headers
//     );
//     return response;
//   } else {
//     // let newHeaders = refreshToken();
//     // console.log('newHeaders', newHeaders);
//     console.log('failed interceptor response');
//     return response;
//   }
// },
// error => {
//   console.log('wow', error);
// // Return any error which is not due to authentication back to the calling service
// if (error.response.status !== 401) {
//   return new Promise((resolve, reject) => {
//     reject(error);
//   });
// }
//
// // Logout user if token refresh didn't work or user is disabled
// if (
//   error.config.url == '/api/token/refresh' ||
//   error.response.message == 'Account is disabled.'
// ) {
//   TokenStorage.clear();
//   router.push({ name: 'root' });
//
//   return new Promise((resolve, reject) => {
//     reject(error);
//   });
// }
//
// // Try request again with new token
// return TokenStorage.getNewToken()
//   .then(token => {
//     // New request with new token
//     const config = error.config;
//     config.headers['Authorization'] = `Bearer ${token}`;
//
//     return new Promise((resolve, reject) => {
//       axios
//         .request(config)
//         .then(response => {
//           resolve(response);
//         })
//         .catch(error => {
//           reject(error);
//         });
//     });
//   })
//   .catch(error => {
//     Promise.reject(error);
//   });
// }
// ();

const coreRepository = axios.create({
  baseURL: coreUrl,
  headers: {
    'Content-Type': 'application/json',
    Source: 'rest-api',
    Accept: 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token'),
    uid: localStorage.getItem('uid'),
  },
});

// coreRepository.interceptors.response.use(response =>
//   refreshToken(coreRepository, response)
// );

const outletRepository = axios.create({
  baseURL: outletUrl,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

const notificationsRepository = axios.create({
  baseURL: notificationsUrl,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

const serviceMessagesRepository = axios.create({
  baseURL: serviceMessagesUrl,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

const calendarRepository = axios.create({
  baseURL: calendarUrl,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

const attendanceRepository = axios.create({
  baseURL: attendanceUrl,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});
const acquiringRepository = axios.create({
  baseURL: acquiringUrl,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

const documentRepository = axios.create({
  baseURL: coreUrl,
  headers: {
    'Content-Type': 'multipart/form-data',
    Source: 'rest-api',
    Accept: 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token'),
    uid: localStorage.getItem('uid'),
  },
});
const commonRepository = axios.create({
  baseURL: commonUrl,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});
export {
  acquiringRepository,
  attendanceRepository,
  documentRepository,
  //Тесты удалить!!!
  authRepository,
  coreRepository,
  outletRepository,
  serviceMessagesRepository,
  notificationsRepository,
  calendarRepository,
  commonRepository,
};
