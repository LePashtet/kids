import AuthRepository from './authRepository';
import { asyncSetLocalStorage } from '@/helpers/methods/asyncLocalStorage';
import router from '@/router/router';

// import { router } from '@/app.vue';

export default async (repository, response) => {
  const originalRequest = response.config;
  if (response.data.status != 'ok' && !originalRequest._retry) {
    originalRequest._retry = true;
    const resp = await AuthRepository.refresh({
      refresh_token: localStorage.getItem('refresh_token'),
      token: localStorage.getItem('token'),
      uid: localStorage.getItem('uid'),
    });
    if (resp.data.status == 'ok') {
      {
        // 1) put token to LocalStorage
        await asyncSetLocalStorage(
          'refresh_token',
          resp?.data?.data?.refresh_token
        );
        await asyncSetLocalStorage('token', resp?.data?.data?.token);
        await asyncSetLocalStorage('uid', resp?.data?.data?.uid);

        // 2) Change Authorization header
        repository.defaults.headers['Authorization'] =
          (await 'Bearer ') + (await resp?.data?.data?.token);
        repository.defaults.headers['uid'] = await resp?.data?.data?.uid;

        originalRequest.headers = {
          Authorization: 'Bearer ' + resp?.data?.data?.token,
          uid: resp?.data?.data?.uid,
        };
        // originalRequest.headers['Authorization'] =
        //   'Bearer ' + resp?.data?.data?.token;
        // originalRequest.headers['uid'] = resp?.data?.data?.uid;

        // 3) return originalRequest object with Axios.
        return repository(originalRequest);
      }
    }
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('role');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('token');
    localStorage.removeItem('uid');
    router.push({ name: 'home' });
    // return Error object with Promise
    return Promise.reject('error');
  } else {
    return response;
  }
};
