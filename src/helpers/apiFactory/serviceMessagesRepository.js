import { serviceMessagesRepository } from './Repository';

const category = '/service-message-category';
const messages = '/service-messages';

export default {
  addCategory(payload) {
    return serviceMessagesRepository.post(`${category}/create`, payload);
  },
  getCategories(payload) {
    return serviceMessagesRepository.get(`${category}/index`, {
      params: { ...payload },
    });
  },
  addMessage(payload) {
    return serviceMessagesRepository.post(`${messages}/create`, payload);
  },
  getMessages(payload) {
    return serviceMessagesRepository.get(`${messages}/index`, {
      params: { ...payload },
    });
  },
};
