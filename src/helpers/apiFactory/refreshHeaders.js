import {
  coreRepository,
  // outletRepository,
  // notificationsRepository,
  // serviceMessagesRepository,
  // calendarRepository,
} from './Repository';

export default {
  refresh(token, uid) {
    return new Promise(resolve => {
      coreRepository.defaults.headers.Authorization = 'Bearer ' + token;
      coreRepository.defaults.headers.uid = uid;

      // outletRepository.defaults.headers['Authorization'] = 'Bearer ' + token;
      // outletRepository.defaults.headers['uid'] = uid;
      //
      // notificationsRepository.defaults.headers['Authorization'] =
      //   'Bearer ' + token;
      // notificationsRepository.defaults.headers['uid'] = uid;
      //
      // serviceMessagesRepository.defaults.headers['Authorization'] =
      //   'Bearer ' + token;
      // serviceMessagesRepository.defaults.headers['uid'] = uid;
      //
      // calendarRepository.defaults.headers['Authorization'] = 'Bearer ' + token;
      // calendarRepository.defaults.headers['uid'] = uid;
      resolve();
    });
  },
};
