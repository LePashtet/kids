import { notificationsRepository } from './Repository';

export default {
  getNotificationsList(payload) {
    return notificationsRepository.get(``, {
      params: { ...payload },
    });
  },
};
