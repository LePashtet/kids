import { authRepository } from './Repository';
import { coreRepository } from './Repository';

const resource = '/auth';
const company = '/company';
export default {
  login(payload) {
    return authRepository.post(`${resource}/login`, payload);
  },
  refresh(payload) {
    return authRepository.post(
      `${resource}/refresh`,
      JSON.stringify({ ...payload }),
      {
        headers: { uid: payload.uid },
      }
    );
  },
  registerUser(payload) {
    return authRepository.post(`${resource}/signup`, payload);
  },
  registerKid(payload) {
    return coreRepository.post(`${resource}/children-signup`, payload);
  },
  createKid(payload) {
    return coreRepository.post(`${resource}/signup-child`, payload);
  },
  changePassword(payload) {
    return authRepository.post(`${resource}/changepassword`, payload);
  },
  recoverPassword(payload) {
    return authRepository.post(`${resource}/newpassword`, payload);
  },
  registerObject(payload) {
    return coreRepository.post(`${company}/create`, payload);
  },
  emailConfirm(payload) {
    return authRepository.post(`${resource}/token`, payload);
  },
  emailConfirmToken(payload) {
    return authRepository.post(`${resource}/mailtoken`, payload);
  },
};
