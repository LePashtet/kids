import { attendanceRepository, coreRepository } from './Repository';

export default {
  getPresence(payload) {
    return attendanceRepository.get(`attendance/presence`, { params: payload });
  },
  getList(payload) {
    return coreRepository.get('/group/view-users', { params: payload });
  },
  setPresence(payload) {
    return attendanceRepository.post('attendance/mark', payload);
  },
  setClientPresence(payload) {
    return attendanceRepository.post('attendance/secure-mark', payload);
  },
  getPresenceReport(payload, params) {
    return attendanceRepository.post('attendance/report-user', payload, {
      params: { ...params },
    });
  },
};
