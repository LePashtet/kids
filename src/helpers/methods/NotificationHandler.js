const componentNotificationHandler = function(that, title, text, status) {
  let notification = {};
  notification.title = title;
  notification.text = text;
  notification.notificationType = status;
  notification.status = 'new';
  that.$store.dispatch('notifications/addNewNotification', notification);
  return true;
};
export { componentNotificationHandler };
