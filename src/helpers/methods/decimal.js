export const decimal = {
  data() {
    return {};
  },
  methods: {
    formatDecimals(val) {
      val = val + '';
      if (val.split('').includes(',')) {
        let before = val.split(',')[0];
        let after =
          val.split(',')[1].length < 3
            ? val.split(',')[1]
            : val.split(',')[1].substr(0, 2);
        return parseFloat(`${before}.${after}`);
      } else if (val.split('').includes('.')) {
        let before = val.split('.')[0];
        let after =
          val.split('.')[1].length < 3
            ? val.split('.')[1]
            : val.split('.')[1].substr(0, 2);
        return parseFloat(`${before}.${after}`);
      } else if (isNaN(val) || val.length == 0) {
        return null;
      } else {
        return parseFloat(val);
      }
    },
  },
};
