const storeErrorHandler = function(dispatch, resp, title, text) {
  let notification = {};
  notification.title = title;
  notification.text = `${text} (${resp.data.message})`;
  notification.notificationType = 'error';
  notification.status = 'new';
  dispatch('notifications/addNewNotification', notification, { root: true });
  return true;
};

const componentErrorHandler = function(that, title, text, status, error) {
  let notification = {};
  notification.title = title;
  let errorMessage = error ? `(${error})` : '';
  notification.text = `${text} ${errorMessage}`;
  notification.notificationType = status;
  notification.status = 'new';
  that.$store.dispatch('notifications/addNewNotification', notification);
  return true;
};
export { storeErrorHandler, componentErrorHandler };
