export class CalendarEvent {
  constructor(event = {}) {
    this.id = event.id;
    this.startDate = event.startDate;
    if (event.endDate) {
      this.endDate = event.endDate;
    }
    this.title = event.title;
    this.url = event.url;
    this.classes = event.classes;
    this.style = event.style;
  }
}
