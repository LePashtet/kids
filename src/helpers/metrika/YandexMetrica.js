import VueYandexMetrika from 'vue-yandex-metrika';
import router from '@/router/router';
import Vue from 'vue';

Vue.use(VueYandexMetrika, {
  id: 62810455,
  router: router,
  scriptSrc: 'https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js',
  env: process.env.NODE_ENV,
  options: {
    clickmap: true,
    trackLinks: true,
    accurateTrackBounce: true,
  },
  // other options
});
