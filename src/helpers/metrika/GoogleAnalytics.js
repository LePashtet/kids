import VueAnalytics from 'vue-analytics';
import router from '@/router/router';
import Vue from 'vue';

// Configuration VueAnalytics
Vue.use(VueAnalytics, {
  id: 'UA-166814989-1',
  router,
});
