import Vue from 'vue';

export const ClickOutsideExcept = {
  bind: function(el, binding, vNode) {
    // Provided expression must evaluate to a function.
    if (typeof binding?.value?.handler !== 'function') {
      const compName = vNode.context.name;
      let warn = `[Vue-click-outside:] provided expression '${binding.expression}' is not a function, but has to be`;
      if (compName) {
        warn += `Found in component '${compName}'`;
      }

      console.warn(warn);
    }
    // Define Handler and cache it on the element
    const bubble = binding.modifiers.bubble;

    const handler = e => {
      const excludedEl = vNode?.context?.$refs[binding?.value?.exclude];
      if (
        bubble ||
        (!el?.contains(e?.target) &&
          el !== e?.target &&
          !excludedEl?.contains(e?.target) &&
          excludedEl !== e?.target)
      ) {
        binding.value.handler(e);
      }
    };
    el.__vueClickOutsideExcept__ = handler;

    // add Event Listeners
    document.addEventListener('click', handler);
  },

  unbind: function(el) {
    // Remove Event Listeners
    document.removeEventListener('click', el.__vueClickOutsideExcept__);
    el.__vueClickOutsideExcept__ = null;
  },
};

Vue.directive('click-outside-except', ClickOutsideExcept);

// // This variable will hold the reference to
// // document's click handler
// let handleOutsideClick;
//
// // Vue.directive('closable', {
// export const closable = {
//   bind(el, binding, vnode) {
//     // Here's the click/touchstart handler
//     // (it is registered below)
//     handleOutsideClick = e => {
//       e.stopPropagation();
//       // Get the handler method name and the exclude array
//       // from the object used in v-closable
//       const { handler, exclude } = binding.value;
//
//       // This variable indicates if the clicked element is excluded
//       console.log('exclude', exclude);
//       let clickedOnExcludedEl = false;
//       if (exclude && exclude?.length > 0) {
//         exclude?.forEach(refName => {
//           // We only run this code if we haven't detected
//           // any excluded element yet
//           if (!clickedOnExcludedEl) {
//             // Get the element using the reference name
//             const excludedEl = vnode?.context?.$refs[refName];
//             // See if this excluded element
//             // is the same element the user just clicked on
//             clickedOnExcludedEl = excludedEl?.contains(e.target);
//           }
//         });
//       }
//       console.log('el', el);
//       // We check to see if the clicked element is not
//       // the dialog element and not excluded
//       if (!el.contains(e.target) && !clickedOnExcludedEl) {
//         // If the clicked element is outside the dialog
//         // and not the button, then call the outside-click handler
//         // from the same component this directive is used in
//         vnode.context[handler]();
//       }
//     };
//     // Register click/touchstart event listeners on the whole page
//     document.addEventListener('click', handleOutsideClick);
//     document.addEventListener('touchstart', handleOutsideClick);
//   },
//
//   unbind() {
//     console.log('unbind');
//
//     // If the element that has v-closable is removed, then
//     // unbind click/touchstart listeners from the whole page
//     document.removeEventListener('click', handleOutsideClick);
//     document.removeEventListener('touchstart', handleOutsideClick);
//   },
// };
//
// Vue.directive('closable', closable);

// export const closable = {
//   bind() {
//     this.event = event => this.vm.$emit(this.expression, event);
//     this.el.addEventListener('click', this.stopProp);
//     document.body.addEventListener('click', this.event);
//   },
//   unbind() {
//     this.el.removeEventListener('click', this.stopProp);
//     document.body.removeEventListener('click', this.event);
//   },
//
//   stopProp(event) {
//     event.stopPropagation();
//   },
// };
