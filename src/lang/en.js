import elements from './en/elements';
import messages from './en/messages';
import constants from './en/constants';
import kitchen from './en/kitchen';
import kinder from './en/kinder';
import home from './en/home';
import user from './en/user';

export default {
  elements,
  messages,
  constants,
  kitchen,
  kinder,
  home,
  user,
};
