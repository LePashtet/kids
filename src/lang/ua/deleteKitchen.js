export default {
  creationDate: 'Дата создания',
  ingredients: 'Ингридиенты',
  comment: 'Коментарий',
  recipes: 'Рецепты',
  recipeForOutlet: 'Торговая точка назначения',
  recipeForProduct: 'Товар на ТТ назначения',

  nameNotFound: 'Наименование не найдено',
};
