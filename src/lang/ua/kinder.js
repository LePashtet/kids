export default {
  account: {
    title: 'Аккаунт',
    tabs: {
      bills: 'Счета',
    },
    billsTable: {
      gartenName: 'Сад',
      group: 'Группа',
      date: 'Дата',
      fio: 'ФИО',
      sum: 'Сумма',
      finalPrice: 'Итог',
      sale: 'Скидка',
      visits: 'Посещения',
      doubt: 'Долг',
      workingDays: 'Рабочие дни',
      name: 'Услуга',
    },
  },
  documents: {
    title: 'Документы',
    tabs: {
      Certificate: 'Справки',
      Document: 'Документы',
      Application: 'Доверенность',
    },
  },
};
