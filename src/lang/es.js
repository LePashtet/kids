import elements from './es/deleteElements.js';
import kitchen from './es/deleteKitchen.js';
import messages from './es/messages';
import constants from './es/constants';
import kinder from './es/kinder';
import home from './es/home';
import user from './es/user';

export default {
  home,
  user,
  kinder,
  elements,
  messages,
  constants,
  kitchen,
};
