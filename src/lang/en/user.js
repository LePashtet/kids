export default {
  titles: {
    main: 'My account',
    qr: 'QR codes',
    notifications: 'Notification archive',
    calendar: 'Calendar of events',
    changePassword: 'Change password',
    changePasswordTab: 'Change password',
    profile: 'Edit profile',
    profileTab: 'Profile',
    refill: 'Charge account',
    refillTab: 'Charge',
    bills: 'Cash Flow',
    billsTab: 'Operation History',
    dayLimit: 'Daily limit',
    dayLimitTab: 'Limit',
    restrictions: 'Product restrictions',
    restrictionsTab: 'Control',
    onlineCatalogTab: 'Online catalogue',
    onlineCatalogHistory: 'Purchase',
    onlineCatalogHistoryTab: 'Purchase History',
    categories: 'Categories',
    productsInCategory: 'Products in the category  "{category}": ',
    objectInfo: 'About the facility',
    objectInfoTab: 'About the facility',
    objectGroup: 'Group information',
    objectGroupTab: 'Group',
    objectContacts: 'Contacts',
    objectContactsTab: 'Contacts',
    sendRequest: 'Send request',
    sendRequestTab: 'Request',
    myRequests: 'History of my requests',
    myRequestsTab: 'History',
    application: 'Доверенность',

    menu: {
      camp: 'Camp',
      payment: 'Оплата',
      account: 'Wallet',
      onlineStore: 'Online store',
      object: 'Link with facility',
      serviceMessages: 'Service messages',
      school: 'School',
      kindergarten: 'Kindergarten',
      parents: 'Ваши Родители',
      message: 'Написать',
      info: 'Инвормация',
      alert: 'Жалоба',
      documents: 'Документы',
      calendar: 'Ежедневник',
    },
  },
  fields: {
    birthday: 'Date of birth',
    month: 'Month',
    year: 'Year',
    gender: 'Gender',
    genderValue: {
      male: 'Male',
      female: 'Female',
    },
    name: 'Name',
    secondName: 'Last Name',
    phone: 'Phone',
    email: 'Email',
    city: 'City',

    camp: 'Сamp',
    squad: 'Group',
    room: 'Room',

    oldPassword: 'Old password',
    password: 'New password',
    passwordCheck: 'New password (repeat)',

    category: 'Category',
    title: 'Title',
    message: 'Message',

    dayLimit: 'JCoin daily limit',
    refillJcoin: 'JCoin recharge amount',
    operationType: 'Operation type',
    operation: 'Operation',
    refill: 'Top up',
    writeOff: 'Write-off',
    summ: 'Amount',
    date: 'Date',
    dateFrom: 'Start date',
    dateTo: 'End date',
    markerTT: 'Marker ТТ',
    eventDay: 'Event Day',
  },
  fieldErrors: {
    samePassword: 'Passwords must match',
    fieldRequired: 'This field is required',
    minLength: 'at least {n} characters',
    allFieldsRequired: '* All fields are required!',
  },
  buttons: {
    addKid: 'Add child',
    registerKid: 'Зарегистрировать ребенка',
    goToEvents: 'Go to events',
    goToProduct: 'Go to product',
    pay: 'Pay',
    refill: 'Request a refund',
    apply: 'Apply',
    send: 'Send',
    attach: 'Bind',
    search: 'Find',
    cancel: 'Cancel',
    logout: 'Exit',
    chooseRole: 'Поменять роль',
    journal: 'Журнал',
    childrenList: 'Списки Детей',
    scanQr: 'Сканировать QR',
    ok: 'Ок',
  },
  parents: {
    welcomeText: {
      dear: 'Dear parents,',
      welcome:
        'welcome to your personal account.  Here you can link your child’s account with yours, for this, click on the add child icon in the block on the right. ',
      childInfo:
        'After the linking procedure, you will be able to track your child’s purchases, as well as recharge his account.',
      support:
        'If you have any difficulties within the system, you can always get help by sending an email:',
      supportEmail: 'web.support@justapp.org',
    },
    addNewKid:
      'To search for a child, enter his phone number, first name and last name.  If all three fields match, you can link the child to your account.',
    authorityKid:
      'Для поиска человека введите его телефон, имя и фамилию. При совпадении всех трех полей, вы сможете привязать доверенное лицо для вашего ребенка.',
    kidNotFound: 'Unfortunately we did not find a child with such data',
    userNotFound: 'К сожалению мы не нашли пользователя с такими данными',
    yourKids: 'Your children',
    menu: 'Ваше меню',
  },
  kids: {
    welcome:
      'Welcome to your personal account.  Here you can get all the information on your account.',
    support:
      'If you have any difficulties within the system, you can always get help by sending an email:',
    supportEmail: 'web.support@justapp.org',
  },
  outlets: {
    productSuccessPurchase: 'Product successfully paid',
    youSuccessfullyBought: 'You have successfully acquired',
    bill: 'Bill',
    summ: 'Amount',
  },
  qr: {
    noObject:
      'At the moment, you are not tied to any camp and you have no checking account. You can use the qr only as a business card of our system.',
    qrBill: 'You can use this QR code for payment.',
  },
  notifications: {
    noMessages: 'There are no new notifications',
    archive: 'Go to archive',
  },
  account: {
    aboutJcoin:
      '* Our system uses the point system - JCoin. All payments on site are made by JCoin, pricing and payment information is also available in JCoin. 1 JCoin = 2 euros',
    refillPageWelcome: "On this page, you can refill user's wallet",
    currentlyOnAccount: 'At the moment in his/her account',
    refillInstructions:
      "Enter the amount that you plan to refill the child's account (below will be displayed the amount to be paid including percentage of the payment system) and press 'Pay', you will redirect to the ROBOKASSA page, where you will be asked to choose a payment method.",
    refillSumm: 'Total due',
    commission: '(including commission of the payment system)',
    currency: '€',
    noBills: "Sorry, this user's bills are not found.",
    noDayLimits:
      'You can set a limit on daily expenses of your child. Simply enter the amount in JCoin, which can be spent per day and he/she will not be able to spend more:',
    currentLimit:
      'For the user has a limit of {limit} JCoin per day. Of them spent today {current} JCoin',
    restrictionWelcome:
      'On this page, you can also set a restriction on the purchase of a particular product, or group of products. The item group can be opened by double-clicking.',
    restrictionInfo:
      '* Goods or categories marked in red are under restriction (a child will not be able to buy them)',
    kidNoLimits: 'At the moment you have no restrictions on amount.',
    kidRestrictionWelcome:
      'On this page, you can also see restrictions on the purchase of certain goods, or group of products. The item group can be opened by clicking on the name.',
    operationType: {
      all: 'All',
      refill: 'Charge',
      writeOff: 'Write-off',
    },
    bill: {
      product: 'Product',
      amount: 'Amount',
      price: 'Price',
      sum: 'Total',
    },
    helpers: {
      legal: 'Information about the legal entity',
      legalLink: '/#',
    },
  },
  onlineOutlet: {
    goToProducts: 'Go to products',
    noStore:
      'This user is not tied to the camp, or at the facility no products online.',
    noBills: 'There is no information about the purchased products.',
    checkAndDate: 'Bill № {number} dated  {date}',
    done: 'Done',
    refilled: 'Refund has been requested',
    new: 'New',
    back: 'Go back',
  },
  settings: {
    passwordInstructions:
      '* To change the current password enter the old password and select a new one. The new password must contain at least 6 characters.',
    photoInstructions:
      'Upload a photo in which you are alone and your face is clearly visible.  This is necessary for identification in the camp and the outlet.',
    uploadProfilePhoto: 'Upload profile photo',
  },
  object: {
    noObject: 'Unfortunately this user is not tied to the camp at the moment.',
    noObjectGroup: 'Sorry, no unit information was found.',
  },
  serviceRequest: {
    sendWelcome:
      'On this page you can leave a request for the administration of the camp.  There was a need to call a locksmith, plumber or other specialist, let us know.  You will be able to track the status of the fulfilment of requests on the page "My requests". ',
    sendInstructions:
      'Select the appropriate category, enter a title and text, and send request to the administration.',
    noCamp:
      'Unfortunately the user is not tied to the camp and service messages are not available.',
  },
  footer: {
    subMenu: {
      admin: 'Access for organizations',
      news: 'News',
      brandBook: 'Brand Center',
    },
    selectLanguage: 'Select language:',
    support: 'If you encounter with a technical problem, let us know:',
    supportEmail: 'web.support@justapp.org',
  },
  common: {
    login: 'login',
    camp: 'camp',
    balance: 'balance',
    jcoin: 'JCoin',
  },
};
