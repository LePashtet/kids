export default {
  header: {
    about: 'About us',
    security: 'Security',
    partners: 'For partners',
    rates: 'Тарифы',
    faq: 'FAQs',
    contacts: 'Contacts',
  },
  footer: {
    subMenu: {
      admin: 'Acces for organizations',
      regCompany: 'Регистрация нового юр.лица',
      news: 'News',
      brandBook: 'Brand Center',
    },
    support: 'If you encounter a technical problem, let us know:',
    supportEmail: 'web.support@justapp.org',
  },
  counter: {
    already: 'Нас уже:',
    users: 'пользователей | пользователь | пользователя | пользователей',
    companies: 'компаний | компания | компании | компаний',
  },
  main: {},
  login: {
    title: 'Authorization',
    rememberMe: 'Remember Me',
  },
  register: {
    title: 'Registration',
    titleObject: 'Registration of a child care facility',

    dataRequest: 'Consent to processing ',
    personalData: 'personal data',
    fieldsRequired: 'Required fields',
    parentMessage:
      '* Dear parents, when registering, indicate your details, you can link a child on a personal page after registration.',
    kidMessage:
      '* To register a child, indicate your phone number, date of birth and email address, as well as the email address of the parents to complete the registration.',
    successHover: 'Congratulations, you have successfully registered!',
    successMessage:
      'We sent confirmation email to the address you provided. Follow the link in the email to activate your account.',
  },
  registerObject: {
    step1: 'Step 1 - register a representative',
    step2: 'Step 2 - details of the legal entity',
    step3: 'Step 3 - object name and contacts',
    step4: 'Step 4 - contract',

    multipleInn:
      'We found more than one organization with the specified ID number, select one from the list',
    tempSuccess:
      'Congratulations, you have successfully registered the facility, we have received your application and will soon send you an agreement and further instructions to the specified Email.',
  },
  registerParentsKid: {
    title: 'Registration of application №',
    parentData: 'Parent data',
    kidsData: 'Child data',
    warning:
      '* Dear parents, when registering, indicate your details, you can link a child on a personal page after registration.',
  },
  emailConfirmation: {
    successTitle: 'Your email has been successfully verified!',
    preSuccessLink: 'Your account is ready to go. Now you can follow',
    successLink: 'this link',
    postSuccessLink: 'and log in to continue working.',
    failedTitle: 'An error occurred while trying to confirm email.',
    failedMessage:
      'We could not confirm your email, the token may have expired. Enter your email in the field below and we will send you a confirmation link again.',
  },

  passwordRecovery: {
    title: 'Password Reset',
    instructions:
      'To reset the password, indicate your phone number and last name for identification, as well as the email address where the new password will be sent.',
    warning:
      '* Attention, the specified email will be saved in the system, all the important notifications will be sent there.  If necessary, you can change the email in your account.',
  },

  fields: {
    phoneCode: 'Код',
    phone: 'Phone',
    extPhone: 'Добавочный',
    birthday: 'Date of birth',
    month: 'Month',
    year: 'Year',
    name: 'Name',
    secondName: 'Last Name',
    email: 'Email',
    parent: 'Parent |  parent',
    password: 'Password',
    passwordCheck: 'Password (repeat)',

    kidsName: "Child's Name",
    kidsSecondName: "Child's last name",

    inn: 'INN',
    objectName: 'Company name',
    objectFullName: 'Full company name',
    kpp: 'КПП',
    address: 'Company address',
    bik: 'БИК',
    bankName: 'Bank name',
    account: 'Settlement account',
    objectEmail: 'Company Email',
    campName: 'Commercial name',
    chooseCompany: 'Select organization',
  },
  buttons: {
    orSeparator: 'or',
    login: 'Login',
    register: 'Register',
    registerObject: 'Register a legal entity',
    registerPartner: 'Become a partner',
    next: 'Next',
    search: 'Search',
    send: 'Send',
    collapse: 'collapse',
    expand: 'expand',
    start: 'start',

    forgotPassword: 'Forgot your password?',
    backToLogin: 'Return to authorization',
    backToMain: 'Повернутися на головну',
  },
  fieldErrors: {
    phoneRequired: 'Enter your phone number',
    emailRequired: 'Please enter a valid email',
    innRequired: 'Enter INN',
    innIncorrect: 'INN is not specified correctly',
    bikRequired: 'Укажите БИК',
    bikIncorrect: 'БИК указан не верно',
    samePassword: 'Passwords must match',
    fieldRequired: 'This field is required',
    minLength: 'at least {n} characters',
  },
  about: {
    appForKidTitle: 'App for children and parents',
    appForKidText:
      'Now, with Justapp you can take care of children and teach them to be independent.',
    kidLocationTitle: 'Complete information on the location of your child',
    kidLocationText:
      'Thanks to the app, you always know all the important details about your child, in which room, in which block your child lives. Who are responsible for him and how to contact them. You can call or write a message directly in the system Justapp.',
    kidCareTitle: 'Care at a distance',
    kidCareText:
      'You can see all purchases your child makes, can set a daily limit on spending and limitations on specific purchases. The child has a choice, the rules set by the parents.',
    settingsTitle: 'Flexible settings',
    settingsText:
      'All that you have restricted and banned yesterday, it is now irrelevant? New day and new circumstances? No problem - change the setting at any time and as many times as you need.',
    problemsTitle: 'When there is a domestic problem',
    problemsText:
      "You or your child found out the broken faucet, burned out light bulb. How to fix it? It is simple – using Justapp. Inform the children's institutions about the problem and will receive an automatic notification when its resolved. Now the problems are solved with one click.",
    calendarTitle: 'Event schedule always by the hand',
    calendarText:
      "You are always aware of events that are planned. It could be hike in the mountains or the opportunity to go on optional tours. Participate in a child's life even from afar.",
    contactsTitle: 'The right contacts are always available.',
    contactsText:
      'You need a leader, artistic director or Director. Such contacts are always available in Justapp.',
    moreAhead: 'And many more to come...',
  },
  security: {
    trafficTitle: 'Encryption',
    trafficText:
      'Our website uses the HTTPS extension to the HTTP Protocol to support encryption to enhance security. The data in the HTTPS Protocol are transmitted over the TLS cryptographic protocols. HTTPS protocol is used to protect confidential data and financial information sent over the Internet. If someone will be able to intercept the message during your HTTPS usage, he would not be able to understand it. Only the sender and the receiver, knowing the encryption system can decrypt the message.',
    trafficListName: 'HTTPS affects your privacy in several ways:',
    trafficList: [
      'It checks whether the website is the one the server needs to contact with.',
      'It prevents changes by other users.',
      'It makes your website more confidential for users.',
      'It encrypts any communication, including URLS that provide things like browsing history and credit card numbers.',
    ],
    personalDataTitle: 'Personal data',
    personalDataText:
      'We are committed to the storage of personal data of our users. The conditions of storage and handling comply with the legislation of the countries of presence. Decentralized data storage, and its use is limited to roles and user permissions.',
    qrTitle: 'Generating QR codes',
    qrText:
      'Our system uses external communication with the help of QR technologies. At first glance, it seems simple and unprotected, but our company took care of security. The system provides the ability to generate QR codes for security transactions. Single-use codes to eliminate the possibility of theft QR, while ensuring accessibility and ease of use.',
  },
  partner: {
    tabs: {
      kindergarten: 'для детского сада',
      school: 'для школы',
      camp: 'для лагеря',
    },
    camp: {
      automationTitle: 'Workflow automation for camps',
      automationText:
        'To organize a tour, to quickly learn about problems, to convey information to parents and children, to accept payments without using cash. Those are not the only things that a free Justapp system  helps to organize.',
      eventsTitle: 'Organization of events',
      eventsText:
        'Has never been so simple to tell children and parents about the upcoming event. A field trip, Neptune festival or the ability to purchase photos for the memory – easy! Just create the event in the "Calendar of events" and it will instantly be known to all adults and children concerned.',
      notificationTitle: 'Rapid notification of all problems',
      notificationText:
        'There will be no more disturbances on intractable problems with a dripping tap or blown light bulb. Children or parents can report the problem. The administration receive the first hand information about the existing problem and the ability to solve it.',
      paymentTitle: 'Payment without cash',
      paymentText:
        'No more money is wasted or lost. Children make purchases using a QR code, it is simple and very reliable. You are involved in leisure of children, not the searching for the missing money.',
      purchaseTitle: 'Parents decide what to buy!',
      purchaseText:
        "Children always want to buy everything, especially when parents are not around. Not all the products are allowed to purchase by the child. Children might have allergies, restrictions from parents or special relationship with certain products and services.This is not your headache anymore. Now parents can prohibit or allow the child's purchases through a mobile application.",
      accessTitle: 'Direct access to clients',
      accessText:
        'Notify children and parents about important events and promotions with Justapp PUSH notifications.',
    },
    school: {
      skudTitle: 'Автоматизация СКУД',
      skudSubTitle: '(система контроля и управления доступом)',
      skudText:
        'При входе посетителя в школу персоналу отображается информация по входящему человеку. Система сообщит, есть ли задолженность по оплате, закроет доступ по индивидуальным настройкам школы. Автоматизирован расчет (перерасчет) стоимости обучения по каждому ученику.',
      messengerTitle: 'Мессенджер для общения',
      messengerText:
        'Для родителей и учителей в мессенджере Justapp созданы закрытые группы. Доступна массовая рассылка информации. Дети комфортно интегрируются в класс и общаются с между собой. ',
      paymentTitle: 'Оплата за обучение и дополнительные услуги',
      paymentText:
        'Получать оплату за обучение или дополнительные услуги – легко и быстро. Родители оплачивают банковской картой через приложение, деньги поступают на расчетный счет школы. Зачисление в системе Justapp происходит мгновенно.',
      tradeTitle: 'Организация торговли без наличных денег',
      tradeText:
        'Школы организует торговлю товарами и услугами без использования наличных денег. Кошелек ребенка идентифицируется благодаря QR-коду, нет никакой возможности потерять деньги или потратить их на что-то ненужное. Родители контролируют покупки ребенка удаленно.',
      scheduleTitle: 'Расписание и журнал',
      scheduleText:
        'Электронный журнал максимально прост и интуитивно понятен для родителей и учителей. Задания, оценки и расписание в едином месте, больше нет ошибок и путаницы. Существует выбор систем оценивания успехов ребенка – 5-ти бальная, 12-ти бальная, буквенная, цветовая и т.д.',
      serviceTitle: 'Сервисные сообщения',
      serviceText:
        'Они помогают администрации школы узнавать о возникающих сложностях: перегорела лампа, утеряна или найдена вещь, конфликт с учителем. Сообщения напрямую попадают ответственному лицу, видны только ему и позволяют решить возникающие задачи в самый короткий срок.',
      docsTitle: 'Электронные документы',
      docsText:
        'Электронная подача документов сокращает сроки работы. Оригиналы документов появятся позже, но сама работа уже выполнена.',
      classesTitle: 'Дополнительные занятия',
      classesText:
        'Кружки, экскурсии и другие мероприятия в едином месте. Родители получат сообщения о новых занятиях или напоминания о текущих. Учет дополнительных занятий также автоматизирован.',
    },
    kindergarten: {
      automationTitle: 'Автоматизация учета посещений',
      automationText:
        'В детском саду происходит учет посещения ребенка воспитателем в системе или сканированием QR-кода в телефоне родителя. Расчет стоимости обучения упрощается, дает важную статистику и уменьшает возможность ошибок.',
      messengerTitle: 'Мессенджер для общения',
      messengerText:
        'Общение родителей и воспитателей в мессенджере Justapp помогает оперативно решить текущие вопросы в общих группах или поговорить с воспитателем наедине.',
      paymentTitle: 'Оплата за обучение и дополнительные услуги',
      paymentText:
        'Прием оплаты в одно касание из мобильного приложения. Достаточно добавить карту и подтвердить оплату. Зачисление в Justapp происходит мгновенно, деньги в полном объеме поступают на расчетный счет детского сада.',
      tradeTitle: 'Организация торговли без наличных денег',
      tradeText:
        'Школы организует торговлю товарами и услугами без использования наличных денег. Кошелек ребенка идентифицируется благодаря QR-коду, нет никакой возможности потерять деньги или потратить их на что-то ненужное. Родители контролируют покупки ребенка удаленно.',
      scheduleTitle: 'Расписание и журнал',
      scheduleText:
        'В электронном журнале отмечаются успехи деток одной из многих систем оценивания – балы, буквы, цветовые отметки и т.д. Расписание содержит информацию по распорядку дня, теперь нет ошибок и путаницы, ведь вся информация собрана в одном месте и отображается сразу после ее внесения.',
      serviceTitle: 'Сервисные сообщения',
      serviceText:
        'Жалоба на воспитателя, перегоревшая лампочка или капающий кран – ничто не останется без внимания администрации сада. Сообщение попадет ответственному сотруднику и не даст мелкому событию перерасти в нечто большее. Родителю будет дана обратная связь.',
      docsTitle: 'Электронные документы',
      docsText:
        'Больше не надо ждать справки о болезни ребенка или оригинала заявления от родителя об отсутствии ребенка. Электронные копии можно передать через Justapp, а оригинал принести при удобном случае.',
      classesTitle: 'Дополнительные занятия',
      classesText:
        'Дети индивидуальны и им нужны разные дополнительные занятия – всевозможные кружки, экскурсии. Теперь родители легко найдут то, что им нужно в общем списке занятий, а вы получите дополнительные поступления и автоматический учет.',
    },
  },
  rates: {
    test: {
      title: 'Пробный',
      discount: '',
      monthly: '0',
      price: '0',
      period: '-',
      tax: '3',
      status: 'Действует первые 14 дней',
    },
    month: {
      title: 'месяц',
      discount: '',
      monthly: '1899',
      price: '1899',
      period: '1 м',
      tax: '3',
      status: 'постоянный тариф',
    },
    quarter: {
      title: 'квартал',
      discount: 'СКИДКА 10%',
      monthly: '1699',
      price: '5097',
      period: '3 м',
      tax: '3',
      status: 'постоянный тариф',
    },
    year: {
      title: 'год',
      discount: 'СКИДКА 21%',
      monthly: '1499',
      price: '17988',
      period: '12 м',
      tax: '3',
      status: 'постоянный тариф',
    },
    special: {
      title: 'специальный',
      discount: '',
      monthly: '0',
      price: '0',
      period: '-',
      tax: '3',
      status: 'автоматически при обороте более 300 000',
    },
    general: {
      monthly: 'абонплата в месяц',
      price: 'абонплата ЗА ПЕРИОД',
      period: 'Период абонплаты',
      tax: 'Комиссия за интернет-эквайРин',
      currency: '$',
    },
  },
  faq: {
    question1: {
      title: 'Как заключить договор?',
      question:
        'Услуги оказываются на основании договора присоединения, который будет доступен в электронном виде в момент регистрации. Регистрируясь, вы принимаете его условия и присоединяетесь к нему.',
    },
    question2: {
      title: 'Что это за пакет «Пробный», почему нет оплаты?\n',
      question:
        'Пакет «Пробный» создан специально для того, чтобы без затрат опробовать новый сервис. В пробный период Вам доступны все функции Justapp.\n',
    },
    question3: {
      title:
        'Хотим пользоваться пакетом «Специальный», что для этого необходимо?\n',
      question:
        'Пакет Специальный подключается автоматически когда поступление платежей через интернет-эквайринг превышает 300 000 рублей в месяц на один объект.\n',
    },
  },

  contacts: {
    amsterdam: 'Amsterdam, Netherlands',
    newYork: 'New York, USA',
    moscow: 'Moscow, Russia',
    kyiv: 'Kyiv, Ukraine',
    nurSultan: 'Nur-Sultan, Kazakhstan',
  },
};
