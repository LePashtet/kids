export default {
  /////////////////////////////////////////
  // Hovers
  authorization: 'Authorization',
  registration: 'Register',
  passwordRecovery: 'Password reset',
  productCatalog: 'product сatalog',
  newProductAdding: 'Adding a new product',
  editProduct: 'Editing product',
  newBarcodeAdding: 'Adding a new bar-code',

  cooking: 'cooking',

  searchBy: 'Search by:',
  /////////////////////////////////////////

  /////////////////////////////////////////
  // auth
  login: 'Login',
  register: 'Register',
  rememberMe: 'Remember me',
  forgotPassword: 'Forgot your password?',
  name: 'Name',
  secondName: 'Second name',
  email: 'Email',
  phone: 'Phone',
  password: 'Password',
  passwordRepeat: 'Repeat password',
  oldPassword: 'Old password',
  send: 'Send',
  returnToLogin: 'Back to Login page',
  scan: 'Scan QR',

  //Register
  birthdayDay: 'Date of birth',
  birthdayMonth: 'Month',
  birthdayYear: 'Year',
  gender: 'Gender',
  city: 'City',

  //Shop
  check: 'check',
  barCode: 'bar code',
  title: 'title',
  makeRefund: 'make a refund',
  makeSale: 'sale',
  totalWithoutDiscount: 'total without discount',
  pay: 'pay',
  cancel: 'cancel',
  applyReturn: 'Refund',
  reset: 'reset',
  apply: 'apply',
  confirm: 'confirm',
  comment: 'comment',
  product: {
    name: 'Name of item',
    priceIn: 'Incoming price',
    priceOut: 'Outcoming price',
    discount: 'Discount',
    barCode: 'Bar code',
    barCodes: 'Bar codes',
    category: 'Category',
    parentCategory: 'Parent category',
    qty: 'Quantity',
    article: 'Article',

    editCategory: 'Change category to',
    editParentCategory: 'Change parent category to',

    addNewProduct: 'Add new item',
    addForCategory: 'Add',
    addInForCategory: 'catefory in',
    applyForCategory: 'Apply',
  },

  shop: {
    title: 'Title',
    price: 'Price',
    priceIn: 'Price in',
    priceOut: 'Price out',
    totalPriceIn: 'Summ in',
    oldPrice: 'Present price',
    newPrice: 'New price',
    summ: 'Sum of order:',
    result: 'Result',
    qty: 'Quantity',
    from: 'From',
    buyerId: 'Buyer ID',
    orderNumber: 'Order №',
    checkNumber: 'Check Number',
    provider: 'Provider',
  },
  status: {
    header: 'Status',
    Active: 'Active',
    Disabled: 'Disabled',
    Archived: 'Archived',
  },

  pagination: {
    page: 'Page',
    items: 'Items',
    out: 'out',
  },

  currency: {
    chooseCurrency: 'Выбор валюты',
  },

  list: 'list',
  withCategories: 'with categories',

  sale: 'sale',
  return: 'return',
  saleList: 'list of sales',
  saleListItem: 'sale order',
  acceptance: 'acceptance',
  acceptanceList: 'list of acceptance',
  acceptanceListItem: 'acceptance order',
  writeOff: 'write-off',
  writeOffList: 'list of write-offs',
  writeOffListItem: 'write-off order',
  reserve: 'reserve',
  overcharge: 'revaluation of goods',
  overchargeList: 'list of revaluation',
  overchargeListItem: 'revaluation order',

  // Security
  securityMain: 'Сканер',
  securityDebtors: 'Должники',

  // KITCHEN
  kitchenAcceptanceListItem: 'отчет о приеме',
  kitchenWriteOffListItem: 'отчет о списании',
  kitchenAcceptance: 'Прием товара',
  kitchenWriteOff: 'Списание товара',
  kitchenRecipe: 'Рецепты',
  kitchenAcceptanceList: 'Список докуметов приема товара',
  kitchenWriteOffList: 'Список докуметов списания товара',

  personalData: 'Consent to the processing of your personal data.',
  /////////////////////////////////////////

  /////////////////////////////////////////
  //home
  loginForRegisteredUser: 'Login for registered users',

  advantagesTitle: 'Просто приложение. Просто для всей семьи ... и не только ',

  advantagesFirstBlockTitle: 'Для детей',
  advantagesFirstBlockContent:
    'Виртуальный кошелек всегда с собой для доступа к развлечениям и возможности радовать себя покупками ...',
  advantagesSecondBlockTitle: 'Для родителей',
  advantagesSecondBlockContent:
    'Ненавязчивый родительский контроль трат и занятий ребенка на расстоянии ...',
  advantagesThirdBlockTitle: 'Для организаций',
  advantagesThirdBlockContent:
    'Администрация объектов получит доступ в режиме реального времени ко всей необходимой информации и аналитике...',

  advantagesBottomText:
    'Быть родителем это счастье, но в тот же момент это огромная ответственность. Отправляя ребенка в лагерь или в школу, родители стремятся обеспечить их карманаными деньгами, чтобы ребенок не чувствовал себя ущемленным и в любой момент мог порадовать себя сладостями или перекусить. Но, как избавиться от мыслей о том, как ребенок распорядится своими средствами, сможет уберечь себя от краж, не потеряет ли их?',
  advantagesBottomText1:
    'Как помочь родителям избавиться от постоянных звонков с целью узнать, как дела и чем занимаются их дети? Как не превратить заботу в навязчивый контроль и постоянные отчеты по телефону?',
  advantagesBottomText2:
    'Мы предоставляем единую платформу для родителей и детей. Наша система позволяет избавиться от наличных расчетов - достаточно пополнить счет ребенка и отслеживать историю платежей, а в случае необходимости вводить ограничения на определенные группы товара либо лимиты на расходы.',
  advantagesBottomTitle:
    'Justapp – уникальное приложение разработанное для решения этих проблем.',

  scopeTitle: 'Приложение Justapp позволит Вам ',
  scopeItem1: 'Осуществлять контроль трат и пополнять ребенку счет',
  scopeItem2: 'Получать информацию о расписании ребенка',
  scopeItem3: 'Надежный доступ к контактам вожатых и преподавателей',
  scopeItem4: 'Всегда знать о перемещениях ребенка',
  scopeItem5: 'Встроеный чат для общения с ребенком и администрацией',

  childHeader: 'После регистрации ребенок получает',
  childItem1:
    'Виртуальный кошелек всегда с собой для доступа к развлечениям и возможности радовать себя покупками',
  childItem2:
    'Чат для общения со сверстниками. Расстояние не помеха, поддерживать связь после каникул станет еще проще.',
  childItem3:
    'Доступ к информации и расписанию лагеря. Все что должен знать и помнить ребенок теперь на экране его телефона.',
  childItem4:
    'Инструмент для комфортного общения с вожатыми и администрацией лагеря. Даже самый стеснительный ребенок сможет задать волнующий его вопрос или сообщить о проблеме.',

  userDescription:
    'Пользователи системы (дети и родители) могут получить доступ в систему через мобильное приложение iOS/Android либо через веб версию. Интуитивно понятный даже ребенку интерфейс позволит пользоваться приложением без подготовки.',
  userItem5:
    'Мы предлагаем безопасную социальную сеть, которая будет контролироваться и модерироваться администрацией.',
  userItem6:
    'Регистрация в сети будет доступна несовершеннолетним, что говорит о цензуре и контроле публичных страниц, запрете на пропаганду алкоголя, курения и т.д.',
  userItem7:
    'Наша служба поддержки готова оказать помощь 7 дней в неделю, 24 часа в сутки.',

  howItWorksTopParents1: 'Пополняют аккаунт наличными на территории объекта',
  howItWorksTopParents2:
    'Или пополняют картой на сайте, либо через терминалы Justapp',
  howItWorksTopParentsTitle: 'Родители',
  howItWorksTopKids1: 'Электронные средства поступают на аккаунт ребенка',
  howItWorksTopKids2:
    'Тратят электронные средства только на территории объекта',
  howItWorksTopKidsTitle: 'Дети',
  howItWorksBottom1:
    'Пополнить счет ребенка можно в любой момент, не выходя из дома. История всех операций со счетом доступны детям и их родителям, ограничения по платежам либо товарным группам настраиваются в любой момент.',
  howItWorksBottom2:
    'Ребенок чувствует себя самостоятельным, принимая решения о покупках и оплачивая их самостоятельно. В тоже время мы предоставляем родителям все необходимые инструменты для контроля и, в случае необходимости, ограничений по платежам. ',

  footerAppTitle: 'Приложение Justapp, уже совсем скоро!',
  footerAppInfo:
    'Justapp – приложение, которое сохранит ваши финансы, нервы и самые памятные знакомства и впечатления. Пополнение кошелька в любой удобный момент экономит время, оставляя его на общение. Самая полезная социальная сеть гарантирует честные финансовые переводы и позитивные воспоминания об отдыхе.',

  paymentsTitle: 'Как работает система платежей в',

  /////////////////////////////////////////

  /////////////////////////////////////////
  //Other text elements
  or: 'or',

  /////////////////////////////////////////

  // Leader
  leaderMain: 'Журнал Посещений',
  leaderList: 'Список Детей',
};
