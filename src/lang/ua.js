import elements from './ru/deleteElements.js';
import kitchen from './ru/deleteKitchen.js';
import messages from './ua/messages';
import constants from './ua/constants';
import kinder from './ua/kinder';
import home from './ua/home';
import user from './ua/user';

export default {
  home,
  user,
  kinder,
  elements,
  messages,
  constants,
  kitchen,
};
