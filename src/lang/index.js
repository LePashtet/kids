import ru from './ru';
import ua from './ua';
import en from './en';
import es from './es';

export default {
  ru,
  ua,
  en,
  es,
};
