export default [
  { abr: 'ru', title: 'Русский', enabled: true },
  { abr: 'ua', title: 'Українська', enabled: true },
  { abr: 'en', title: 'English', enabled: true },
  { abr: 'es', title: 'Español', enabled: true },
  { abr: 'nl', title: 'Nederlands', enabled: false },
  { abr: 'kz', title: 'Қазақ', enabled: false },
  { abr: 'de', title: 'Deutsch', enabled: false },
];
