export default {
  header: {
    about: 'Sobre nosotros',
    security: 'Seguridad',
    partners: 'Empresas',
    rates: 'Тарифы',
    faq: 'FAQs',
    contacts: 'Contactos',
  },
  footer: {
    subMenu: {
      admin: 'Acceso para centros',
      regCompany: 'Регистрация нового юр.лица',
      news: 'Noticias',
      brandBook: 'Imagen corporativa',
    },
    support: 'Si tiene un problema técnico, infórmenos:',
    supportEmail: 'web.support@justapp.org',
  },
  counter: {
    already: 'Нас уже:',
    users: 'пользователей | пользователь | пользователя | пользователей',
    companies: 'компаний | компания | компании | компаний',
  },
  main: {},
  login: {
    title: 'Área privada',
    rememberMe: 'Recordar usuario',
  },
  register: {
    title: 'Crear nuevo usuario',
    titleObject: 'Registro de una institución de cuidado infantil.',

    dataRequest: 'Consentimiento para el procesamiento ',
    personalData: 'de datos personales',
    fieldsRequired: 'Campos obligatorios',
    parentMessage:
      '* Estimados padres, al registrarse, especifique sus datos. Puede vincular a su hijo/a en su página personal después de registrarse.',
    kidMessage:
      '* Para registrar a su hijo/a, indique su número de teléfono de Usted, fecha de nacimiento y dirección de correo electrónico, así como la dirección de correo electrónico de los padres para completar el registro.',
    successHover: 'Congratulations, you have successfully registered!',
    successMessage:
      'We sent confirmation email to the address you provided. Follow the link in the email to activate your account.',
  },
  registerObject: {
    step1: 'Paso 1 - registro del representante',
    step2: 'Paso 2 - requisitos de la persona jurídica',
    step3: 'Paso 3 - nombre de entidad y contactos',
    step4: 'Paso 4 - contrato',

    multipleInn:
      'Hemos encontrado varios centros con el NIF especificado, seleccione uno en la lista',
    tempSuccess:
      'Felicitaciones, ha registrado correctamente el centro, hemos recibido su solicitud y le enviaremos pronto un contrato y las instrucciones siguientes al correo electrónico especificado.',
  },
  registerParentsKid: {
    title: 'Registro de solicitud №',
    parentData: 'Datos de los padres',
    kidsData: 'Datos del niño',
    warning:
      '* Estimados padres, al registrarse, especifique sus datos. Puede vincular a su hijo/a en su página personal después de registrarse.',
  },
  emailConfirmation: {
    successTitle: 'Your email has been successfully verified!',
    preSuccessLink: 'Your account is ready to go. Now you can follow',
    successLink: 'this link',
    postSuccessLink: 'and log in to continue working.',
    failedTitle: 'An error occurred while trying to confirm email.',
    failedMessage:
      'We could not confirm your email, the token may have expired. Enter your email in the field below and we will send you a confirmation link again.',
  },

  passwordRecovery: {
    title: 'Solicitar nueva contraseña',
    instructions:
      'Por favor, introduzca su número de teléfono y apellido para su identificación, así como el correo electrónico al que se enviará la nueva contraseña.',
    warning:
      '* Atención: el correo electrónico especificado se guardará en el sistema, todas las notificaciones importantes se enviarán allí. A continuación, podrá cambiar el correo electrónico en su área personal.',
  },

  fields: {
    phoneCode: 'Код',
    phone: 'Teléfono',
    extPhone: 'Добавочный',
    birthday: 'Fecha del nacimiento',
    month: 'Mes',
    year: 'Año',
    name: 'Nombre',
    secondName: 'Apellido',
    email: 'Email',
    parent: 'Padre| del padre',
    password: 'Contraseñas',
    passwordCheck: 'Contraseñas (repetir)',

    kidsName: 'Nombre del niño/a',
    kidsSecondName: 'Apellido del niño/a',

    inn: 'NIF/CIF',
    objectName: 'Razon social',
    objectFullName: 'Razon social completo',
    kpp: 'КПП',
    address: 'Dirección',
    bik: 'БИК',
    bankName: 'Nombre del banco',
    account: 'IBAN',
    objectEmail: 'Email del centro',
    campName: 'Nombre comercial',
    chooseCompany: 'Elije el centro',
  },
  buttons: {
    orSeparator: 'o',
    login: 'Acceder',
    register: 'Registrarse',
    registerObject: 'Registrar una entidad',
    next: 'Siguiente',
    registerPartner: 'Convertirse en un compañero',
    search: 'Buscar',
    send: 'Enviar',
    collapse: '	minimizar',
    expand: 'maximizar',
    start: 'empezar',

    forgotPassword: 'He olvidado mi contraseña',
    backToLogin: 'Volver a intentar',
    backToMain: 'Повернутися на головну',
  },
  fieldErrors: {
    phoneRequired: 'Indique su teléfono',
    emailRequired: 'Indique Email correcto',
    innRequired: 'Indique NIF/CIF',
    innIncorrect: 'NIF/CIF no es válido',
    bikRequired: 'Укажите БИК',
    bikIncorrect: 'БИК указан не верно',
    samePassword: 'Contraseñas deben coincidir',
    fieldRequired: 'El campo es obligatorio',
    minLength: 'mínimo {n} signos',
  },
  about: {
    appForKidTitle: 'Aplicación para los niños y sus padres',
    appForKidText:
      'Ahora, con Justapp, puede cuidar de sus niños y enseñarles a ser independientes.',
    kidLocationTitle: 'Información completa sobre la localización del niño',
    kidLocationText:
      'Gracias a la aplicación, siempre conoce todos los detalles importantes sobre su hijo/a: en qué habitación y en qué edificio vive. ¿Quién es el responsable y cómo contactarlo? Puede llamar o escribir un mensaje directamente en el sistema Justapp.',
    kidCareTitle: 'Cuidado del niño a distancia',
    kidCareText:
      'Puede ver todas las compras de un niño y establecer tanto el límite diario para gastar dinero como las restricciones sobre compras específicas. El niño tiene una opción, pero los padres establecen las reglas.',
    settingsTitle: 'Ajustes flexibles',
    settingsText:
      '¿Todo lo que limitó y prohibió ayer, hoy no es importante? ¿Un nuevo día y otras circunstancias? No es problema - cambie la configuración en cualquier momento y tantas veces como sea necesario.',
    problemsTitle: 'Cuando surge un problema del hogar',
    problemsText:
      'Usted o su niño/a se enteran de un grifo inactivo o una bombilla fundida. ¿Cómo arreglarlo? Es facil con Justapp. Comunique el problema a la administración del centro y se notifique automáticamente su resolución. Ahora los problemas se resuelven con un solo clic.',
    calendarTitle: 'El calendario de eventos está siempre a mano.',
    calendarText:
      'Siempre está al tanto de lo que se planea en vacaciones. Podría ser una excursión a las montañas o una excursión adicional. Participe en la vida de su niño/a incluso desde lejos.',
    contactsTitle: 'Los contactos necesarios siempre están disponibles.',
    contactsText:
      'Necesita el guía, el dirigente artístico o el director. Estos contactos siempre están disponibles en Justapp.',
    moreAhead: 'Y mucho más por venir...',
  },
  security: {
    trafficTitle: 'Cifrado de tráfico',
    trafficText:
      'Nuestro sitio utiliza HTTPS, una extensión del protocolo HTTP que permite el cifrado para mejorar la seguridad. Los datos de HTTPS se transmiten sobre los protocolos de criptografía de TLS. El protocolo HTTPS se utiliza para proteger los datos confidenciales y la información financiera transmitida por Internet. Si alguien logra interceptar un mensaje con HTTPS, no podrá entenderlo. Sólo el remitente y el receptor que conozcan el sistema de cifrado pueden descifrar el mensaje.',
    trafficListName: 'HTTPS afecta a la privacidad de varias maneras:',
    trafficList: [
      'Comprueba si el sitio es el que debe contactar el servidor.',
      'Impide cambios por parte de otros usuarios.',
      'Esto hace que su sitio web sea más confidencial para los usuarios.',
      'Encripta todas las comunicaciones, incluso las URL, que proporcionan información como el historial de navegación y los números de tarjetas de crédito.',
    ],
    personalDataTitle: 'Datos personales',
    personalDataText:
      'Somos responsables del almacenamiento de datos personales de nuestros usuarios. Las condiciones de almacenamiento y procesamiento se ajustan a la legislación de los países de presencia. Los datos se almacenan de forma descentralizada y su uso se limita a las partes y permisos del usuario.',
    qrTitle: 'Generación de QR-códigos',
    qrText:
      'Nuestro sistema utiliza las comunicaciones externas a través de la tecnología QR. A primera vista, parece simple y desprotegida, pero nuestra compañía se encargó de la seguridad. El sistema permite generar códigos QR para garantizar la seguridad de las transacciones. Los códigos de una sola vez eliminan la posibilidad de robar QR al tiempo que proporcionan disponibilidad y facilidad de uso.',
  },
  partner: {
    tabs: {
      kindergarten: 'для детского сада',
      school: 'для школы',
      camp: 'для лагеря',
    },
    camp: {
      automationTitle:
        'Automatización de los procesos de trabajo de los campamentos',
      automationText:
        'Organizar visitas guiadas, conocer rápidamente los problemas, dar información a los padres y a los niños y aceptar pagos sin utilizar dinero en efectivo - no sólo este sino mucah otras cosas ayuda a organizar un sistema Justapp gratuito.',
      eventsTitle: 'Organización de eventos',
      eventsText:
        'Nunca ha sido tan fácil contarles a los niños y a los padres slo que va a pasar. Una excursión, la fiesta de Neptuno o la oportunidad de comprar una foto de recuerdo - ¡fácil! Simplemente crea un evento en el "Calendario de eventos" y lo descubren inmediatamente todos los adultos y los niños a los que se refiere.',
      notificationTitle: 'Alerta rápida de todos los problemas',
      notificationText:
        'Ya no habrá indignaciones por problemas no resueltos con un grifo que gotea o una bombilla fundida. Los hijos o los padres pueden informar rápidamente del problema. La administración recibirá información de primera mano sobre el problema existente y la oportunidad de resolverlo.',
      paymentTitle: 'Pago sin efectivo',
      paymentText:
        'No se perderá el dinero nunca más. Los niños hacen compras con código QR, todo es fácil y muy fiable. Usted se dedica al ocio de los niños, no a la búsqueda de dinero desaparecido.',
      purchaseTitle: 'Los padres deciden qué comprar!',
      purchaseText:
        'Los niños siempre quieren comprar todo, especialmente cuando sus padres están lejos. Pero no todos los alimentos pueden ser comprados a un niño. Hay alergias, prohibiciones de los padres y un trato especial a diferentes bienes y servicios. Esto ya no es su dolor de cabeza. Ahora, los propios padres a través de la aplicación móvil prohíben o permiten la compra al niño.',
      accessTitle: 'Acceso directo a los clientes',
      accessText:
        'Avisa a los hijos y sus padres de eventos, actividades y acciones importantes con las notificaciones PUSH de Justapp.',
    },
    school: {
      skudTitle: 'Автоматизация СКУД',
      skudSubTitle: '(система контроля и управления доступом)',
      skudText:
        'При входе посетителя в школу персоналу отображается информация по входящему человеку. Система сообщит, есть ли задолженность по оплате, закроет доступ по индивидуальным настройкам школы. Автоматизирован расчет (перерасчет) стоимости обучения по каждому ученику.',
      messengerTitle: 'Мессенджер для общения',
      messengerText:
        'Для родителей и учителей в мессенджере Justapp созданы закрытые группы. Доступна массовая рассылка информации. Дети комфортно интегрируются в класс и общаются с между собой. ',
      paymentTitle: 'Оплата за обучение и дополнительные услуги',
      paymentText:
        'Получать оплату за обучение или дополнительные услуги – легко и быстро. Родители оплачивают банковской картой через приложение, деньги поступают на расчетный счет школы. Зачисление в системе Justapp происходит мгновенно.',
      tradeTitle: 'Организация торговли без наличных денег',
      tradeText:
        'Школы организует торговлю товарами и услугами без использования наличных денег. Кошелек ребенка идентифицируется благодаря QR-коду, нет никакой возможности потерять деньги или потратить их на что-то ненужное. Родители контролируют покупки ребенка удаленно.',
      scheduleTitle: 'Расписание и журнал',
      scheduleText:
        'Электронный журнал максимально прост и интуитивно понятен для родителей и учителей. Задания, оценки и расписание в едином месте, больше нет ошибок и путаницы. Существует выбор систем оценивания успехов ребенка – 5-ти бальная, 12-ти бальная, буквенная, цветовая и т.д.',
      serviceTitle: 'Сервисные сообщения',
      serviceText:
        'Они помогают администрации школы узнавать о возникающих сложностях: перегорела лампа, утеряна или найдена вещь, конфликт с учителем. Сообщения напрямую попадают ответственному лицу, видны только ему и позволяют решить возникающие задачи в самый короткий срок.',
      docsTitle: 'Электронные документы',
      docsText:
        'Электронная подача документов сокращает сроки работы. Оригиналы документов появятся позже, но сама работа уже выполнена.',
      classesTitle: 'Дополнительные занятия',
      classesText:
        'Кружки, экскурсии и другие мероприятия в едином месте. Родители получат сообщения о новых занятиях или напоминания о текущих. Учет дополнительных занятий также автоматизирован.',
    },
    kindergarten: {
      automationTitle: 'Автоматизация учета посещений',
      automationText:
        'В детском саду происходит учет посещения ребенка воспитателем в системе или сканированием QR-кода в телефоне родителя. Расчет стоимости обучения упрощается, дает важную статистику и уменьшает возможность ошибок.',
      messengerTitle: 'Мессенджер для общения',
      messengerText:
        'Общение родителей и воспитателей в мессенджере Justapp помогает оперативно решить текущие вопросы в общих группах или поговорить с воспитателем наедине.',
      paymentTitle: 'Оплата за обучение и дополнительные услуги',
      paymentText:
        'Прием оплаты в одно касание из мобильного приложения. Достаточно добавить карту и подтвердить оплату. Зачисление в Justapp происходит мгновенно, деньги в полном объеме поступают на расчетный счет детского сада.',
      tradeTitle: 'Организация торговли без наличных денег',
      tradeText:
        'Школы организует торговлю товарами и услугами без использования наличных денег. Кошелек ребенка идентифицируется благодаря QR-коду, нет никакой возможности потерять деньги или потратить их на что-то ненужное. Родители контролируют покупки ребенка удаленно.',
      scheduleTitle: 'Расписание и журнал',
      scheduleText:
        'В электронном журнале отмечаются успехи деток одной из многих систем оценивания – балы, буквы, цветовые отметки и т.д. Расписание содержит информацию по распорядку дня, теперь нет ошибок и путаницы, ведь вся информация собрана в одном месте и отображается сразу после ее внесения.',
      serviceTitle: 'Сервисные сообщения',
      serviceText:
        'Жалоба на воспитателя, перегоревшая лампочка или капающий кран – ничто не останется без внимания администрации сада. Сообщение попадет ответственному сотруднику и не даст мелкому событию перерасти в нечто большее. Родителю будет дана обратная связь.',
      docsTitle: 'Электронные документы',
      docsText:
        'Больше не надо ждать справки о болезни ребенка или оригинала заявления от родителя об отсутствии ребенка. Электронные копии можно передать через Justapp, а оригинал принести при удобном случае.',
      classesTitle: 'Дополнительные занятия',
      classesText:
        'Дети индивидуальны и им нужны разные дополнительные занятия – всевозможные кружки, экскурсии. Теперь родители легко найдут то, что им нужно в общем списке занятий, а вы получите дополнительные поступления и автоматический учет.',
    },
  },
  rates: {
    test: {
      title: 'Пробный',
      discount: '',
      monthly: '0',
      price: '0',
      period: '-',
      tax: '3',
      status: 'Действует первые 14 дней',
    },
    month: {
      title: 'месяц',
      discount: '',
      monthly: '1899',
      price: '1899',
      period: '1 м',
      tax: '3',
      status: 'постоянный тариф',
    },
    quarter: {
      title: 'квартал',
      discount: 'СКИДКА 10%',
      monthly: '1699',
      price: '5097',
      period: '3 м',
      tax: '3',
      status: 'постоянный тариф',
    },
    year: {
      title: 'год',
      discount: 'СКИДКА 21%',
      monthly: '1499',
      price: '17988',
      period: '12 м',
      tax: '3',
      status: 'постоянный тариф',
    },
    special: {
      title: 'специальный',
      discount: '',
      monthly: '0',
      price: '0',
      period: '-',
      tax: '3',
      status: 'автоматически при обороте более 300 000',
    },
    general: {
      monthly: 'абонплата в месяц',
      price: 'абонплата ЗА ПЕРИОД',
      period: 'Период абонплаты',
      tax: 'Комиссия за интернет-эквайРин',
      currency: '€',
    },
  },
  faq: {
    question1: {
      title: 'Как заключить договор?',
      question:
        'Услуги оказываются на основании договора присоединения, который будет доступен в электронном виде в момент регистрации. Регистрируясь, вы принимаете его условия и присоединяетесь к нему.',
    },
    question2: {
      title: 'Что это за пакет «Пробный», почему нет оплаты?\n',
      question:
        'Пакет «Пробный» создан специально для того, чтобы без затрат опробовать новый сервис. В пробный период Вам доступны все функции Justapp.\n',
    },
    question3: {
      title:
        'Хотим пользоваться пакетом «Специальный», что для этого необходимо?\n',
      question:
        'Пакет Специальный подключается автоматически когда поступление платежей через интернет-эквайринг превышает 300 000 рублей в месяц на один объект.\n',
    },
  },

  contacts: {
    amsterdam: 'Ámsterdam, Países Bajos',
    newYork: 'Nueva York, Estados Unidos',
    moscow: 'Moscú, Rusia',
    kyiv: 'Kyiv, Ucrania',
    nurSultan: 'Nur-Sultan, Kazajstán',
  },
};
