export default {
  titles: {
    main: 'Área privada',
    qr: 'QR - códigos',
    notifications: 'Archivo de notificaciones',
    calendar: 'Calendario de eventos',
    changePassword: 'Cambiar contraseña',
    changePasswordTab: 'Cambiar contraseña',
    profile: 'Editar perfil',
    profileTab: 'Perfil de usuario',
    refill: 'Recargo',
    refillTab: 'Recargar',
    bills: 'Movimientos',
    billsTab: 'Historia de operaciones',
    dayLimit: 'Limite diario',
    dayLimitTab: 'Limite',
    restrictions: 'Restricciones del producto',
    restrictionsTab: 'Control',
    onlineCatalogTab: 'Catalogo online',
    onlineCatalogHistory: 'Compras',
    onlineCatalogHistoryTab: 'Historia de compras',
    categories: 'Categarias',
    productsInCategory: 'Productos en la categoría "{category}": ',
    objectInfo: 'Sobre el centro',
    objectInfoTab: 'Sobre el centro',
    objectGroup: 'Información del grupo',
    objectGroupTab: 'Grupo',
    objectContacts: 'Contactos',
    objectContactsTab: 'Contactos',
    sendRequest: 'Enviar una consulta',
    sendRequestTab: 'Mis consultas',
    myRequests: 'Historia de consultas',
    myRequestsTab: 'Historia',
    application: 'Доверенность',

    menu: {
      camp: 'Campamento',
      payment: 'Оплата',
      account: 'Cartera',
      onlineStore: 'Tienda online',
      object: 'Enlace del centro',
      serviceMessages: 'Mensajes del servicio',
      school: 'Colegio',
      kindergarten: 'Guardería',
      parents: 'Ваши родители',
      message: 'Написать',
      info: 'Инвормация',
      alert: 'Жалоба',
      documents: 'Документы',
      calendar: 'Ежедневник',
    },
  },
  fields: {
    birthday: 'Fecha de nacimiento',
    month: 'Mes',
    year: 'Año',
    gender: 'Genero',
    genderValue: {
      male: 'Masculino',
      female: 'Femenino',
    },
    name: 'Nombre',
    secondName: 'Apellido/os',
    phone: 'Teléfono',
    email: 'Email',
    city: 'Ciudad',

    camp: 'Campamento',
    squad: 'Grupo',
    room: 'Habitación',

    oldPassword: 'Contraseña anterior',
    password: 'Contraseña nueva',
    passwordCheck: 'Contraseña nueva (repetir)',

    category: 'Categoría',
    title: 'Título',
    message: 'Mensaje',

    dayLimit: 'Limite diario JCoin',
    refillJcoin: 'Cantidad de recargo en JCoin',
    operationType: 'Tipo de operación',
    operation: 'Operación',
    refill: 'Recarga',
    writeOff: 'Cargo en cuenta',
    summ: 'Importe',
    date: 'Fecha',
    dateFrom: 'Fecha de inicio',
    dateTo: 'Fecha de finalización',
    markerTT: 'Marcador ТТ',
    eventDay: 'Día del evento',
  },
  fieldErrors: {
    samePassword: 'Contraseñas deben coincidir',
    fieldRequired: 'El campo es obligatorio',
    minLength: 'mínimo {n} signos',
    allFieldsRequired: '* Todos los campos son obligatorios!',
  },
  buttons: {
    addKid: 'Agregar hijo',
    registerKid: 'Зарегистрировать ребенка',
    goToEvents: 'Ir a eventos',
    goToProduct: 'Ir al producto',
    pay: 'Pagar',
    refill: 'Devolución',
    apply: 'Aceptar',
    send: 'Enviar',
    attach: 'Vincular',
    search: 'Buscar',
    cancel: 'Cancelar',
    logout: 'Salir',
    chooseRole: 'Поменять роль',
    journal: 'Журнал',
    childrenList: 'Списки Детей',
    scanQr: 'Сканировать QR',
    ok: 'Ок',
  },
  parents: {
    welcomeText: {
      dear: 'Estimados padres,',
      welcome:
        'bienvenido a su área privada. Aquí puede vincular la cuenta de su hijo con la suya, para ello haga clic en el icono "Agregar hijo" en el bloque de la derecha. ',
      childInfo:
        'Después del procedimiento de vinculación, puede seguir de las compras de su hijo y recargar su cuenta.',
      support:
        'Si tiene alguna dificultad con el sistema, siempre puede obtener ayuda enviando un correo electrónico al email:',
      supportEmail: 'web.support@justapp.org',
    },
    addNewKid:
      'Para encontrar a su hijo, escriba su teléfono, nombre y apellido. Si los tres campos coinciden, puede vincular al niño a su cuenta.',
    authorityKid:
      'Для поиска человека введите его телефон, имя и фамилию. При совпадении всех трех полей, вы сможете привязать доверенное лицо для вашего ребенка.',
    kidNotFound: 'Desafortunadamente, no encontramos a un niño con esos datos',
    userNotFound: 'К сожалению мы не нашли пользователя с такими данными',
    yourKids: 'Sus hijos',
    menu: 'Ваше меню',
  },
  kids: {
    welcome:
      'Bienvenido a su área privada. Aquí puede obtener toda la información de su cuenta.',
    support:
      'Si tiene alguna dificultad con el sistema, siempre puede obtener ayuda enviando un correo electrónico al email:',
    supportEmail: 'web.support@justapp.org',
  },
  outlets: {
    productSuccessPurchase: 'Producto pagado con éxito',
    youSuccessfullyBought: 'Has comprado con exito',
    bill: 'Factura',
    summ: 'Importe',
  },
  qr: {
    noObject:
      'Por el momento, no está conectado a ningún campamento y no tiene una cuenta corriente. Sólo puede utilizar el código QR como tarjeta de visita para nuestro sistema.',
    qrBill: 'Puede utilizar este código QR para pagar.',
  },
  notifications: {
    noMessages: 'No hay notificaciones nuevas',
    archive: 'Ir al archivo',
  },
  account: {
    aboutJcoin:
      '* Our system uses the point system - JCoin. All payments on site are made by JCoin, pricing and payment information is also available in JCoin. 1 JCoin = 2 euros',
    refillPageWelcome: 'En esta página puede recargar la cuenta del usuario',
    currentlyOnAccount: 'Actualmente tiene en su cuenta',
    refillInstructions:
      'Introduzca el importe que planea recargar la cuenta de su hijo/a (a continuación aparecerá el importe del pago en función del porcentaje del sistema de pago) y haga clic en "Pagar", se lo redirige a la página de Robocassa, donde se le pide que elija el método de pago.',
    refillSumm: 'Total a pagar',
    commission: '(la comisión del sistema de pago)',
    currency: '€',
    noBills: 'No se encontraron facturas de este usuario.',
    noDayLimits:
      'Puede establecer un límite para el gasto diario de su hijo/a. Simplemente introduzca una cantidad en JCoin que el/la niño/a puede gastar al día y el/ella no podrá gastar más:',
    currentLimit:
      'El usuario tiene un límite de {limit} JCoin por día. De ellos, hoy se ha gastado {current} JCoin',
    restrictionWelcome:
      'En esta página también puede establecer una restricción para la compra de un artículo específico o de un grupo de productos. Un grupo de artículos se puede abrir con doble clic.',
    restrictionInfo:
      '* Los productos o las categorías marcados en rojo están limitados (el niño no podrá comprarlos)',
    kidNoLimits: 'Por el momento no tiene límites en la cantidad.',
    kidRestrictionWelcome:
      'En esta página también puede ver las restricciones a la compra de un artículo específico o de un grupo de productos. Un grupo de artículos se puede abrir con doble clic.',
    operationType: {
      all: 'Todas',
      refill: 'Recargar',
      writeOff: 'Cargo en cuenta',
    },
    bill: {
      product: 'Producto',
      amount: 'Cantidad',
      price: 'Precio',
      sum: 'Suma',
    },
    helpers: {
      legal: 'Information about the legal entity',
      legalLink: '/#',
    },
  },
  onlineOutlet: {
    goToProducts: 'Ir a productos',
    noStore:
      'Este usuario no está vinculado al campamento o no hay productos en línea en este centro.',
    noBills: 'No hay información sobre los productos comprados por el usuario',
    checkAndDate: 'Factura №{number} de {date}',
    done: 'Cumplido',
    refilled: 'Devolución emitida',
    new: 'Nuevo',
    back: 'Regresar',
  },
  settings: {
    passwordInstructions:
      '* Para cambiar la contraseña actual, escriba la contraseña anterior y seleccione una nueva. La nueva contraseña no debe tener menos de 6 caracteres.',
    photoInstructions:
      'Suba una foto donde esté solo y su cara sea claramente visible. Esto es necesario para la identificación en el campamento y el punto de venta.',
    uploadProfilePhoto: 'Subir foto de perfil',
  },
  object: {
    noObject:
      'Desafortunadamente, este usuario no está conectado al campamento actualmente.',
    noObjectGroup:
      'Lamentablemente, no se ha encontrado información sobre el grupo.',
  },
  serviceRequest: {
    sendWelcome:
      'En esta página puede dejar una solicitud para la administración del campamento si es necesario llamar a un cerrajero, un fontanero u otro especialista. Puede seguir del estado de las solicitudes en "Mis consultas".',
    sendInstructions:
      'Seleccione la categoría apropiada, escriba el título y el texto y envíe la solicitud a la administración.',
    noCamp:
      'Desafortunadamente, el usuario no está vinculado al campamento y los mensajes de servicio no están disponibles.',
  },
  footer: {
    subMenu: {
      admin: 'Acceso para los centros',
      news: 'Noticias',
      brandBook: 'Imagen corporativa',
    },
    selectLanguage: 'Seleccione el idioma:',
    support: 'Si tiene un problema técnico, comuníquenos:',
    supportEmail: 'web.support@justapp.org',
  },
  common: {
    login: 'login',
    camp: 'campamento',
    balance: 'saldo',
    jcoin: 'JCoin',
  },
};
