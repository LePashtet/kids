import elements from './ru/deleteElements.js';
import kitchen from './ru/deleteKitchen.js';
import messages from './ru/messages';
import constants from './ru/constants';
import kinder from "./ru/kinder";
import home from './ru/home';
import user from './ru/user';

export default {
  home,
  user,
  kinder,
  elements,
  messages,
  constants,
  kitchen,
};
