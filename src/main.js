import Vue from 'vue';
import App from './App.vue';
import router from './router/router';
import store from './store/store';
import { i18n } from '@/lang/i18n';
import VueCookies from 'vue-cookies';
import './registerServiceWorker';

import VueHtmlToPaper from 'vue-html-to-paper'; //ToDo убрать локальное подключение

//Use side effects, no imports
import '@/helpers/metrika/YandexMetrica';
import '@/helpers/metrika/GoogleAnalytics';
import './registerServiceWorker';

const options = {
  name: '_blank',
  specs: ['fullscreen=yes', 'titlebar=yes', 'scrollbars=yes', 'width=80mm'],
  styles: ['./css/print.css'],
};
Vue.use(VueHtmlToPaper, options);

Vue.use(VueCookies);
Vue.$cookies.config('30d');

Vue.config.productionTip = false;
Vue.config.performance = true;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app');
