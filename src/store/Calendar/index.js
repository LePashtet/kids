import actions from './actions';
import mutations from './mutations';

export default {
  namespaced: true,
  state: {
    // currentEvent: {},
    // eventsCalendar: [],
    // events: [],
  },
  actions: actions,
  mutations: mutations,
};
