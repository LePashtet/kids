import actions from './actions';
import mutations from './mutations';
import getters from './getters';

export default {
  namespaced: true,
  state: {
    // @todo Поменять хардкод на ответ с бэка
    currencies: [
      {
        id: 1,
        title: 'Рубль',
        abbr: 'Руб',
        ratio: 1,
      },
      {
        id: 2,
        title: 'Just Coin',
        abbr: 'JCoin',
        ratio: 0.5,
      },
    ],
    currentCurrency: {
      id: 1,
      title: 'Рубль',
      abbr: 'Руб',
      ratio: 1,
    },

    urls: {
      outlet: process.env.VUE_APP_API_OUTLET,
      core: process.env.VUE_APP_API_CORE,
      acquiring: process.env.VUE_APP_API_ACQUIRING,
    },
  },
  actions: actions,
  getters: getters,
  mutations: mutations,
};
