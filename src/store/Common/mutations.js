export default {
  CHANGE_CURRENT_CURRENCY(state, id) {
    state.currentCurrency = state.currencies.find(x => x.id === id);
  },
};
