export default {
  changeCurrentCurrency({ commit }, id) {
    commit('CHANGE_CURRENT_CURRENCY', id);
  },
};
