import Vue from 'vue';
export default {
  GET_PRODUCT_STATUSES(state, data) {
    state.productStatuses = data;
  },
  GET_OUTLET_ID(state, data) {
    if (localStorage.getItem('outletID') != data.data.data.id) {
      localStorage.setItem('outletID', data.data.data.id);
    }
  },
  SET_CURRENT_OUTLET(state, data) {
    state.outlet_id = parseInt(data);
  },
  GET_OUTLET_INFO(state, info) {
    state.outletInfo = info.data.data;
  },
  GET_CATEGORY_LIST(state, data) {
    state.productCategories = Object.freeze(data);
  },
  GET_PRODUCTS_LIST(state, data) {
    state.productList = [];
    state.productList = Object.freeze(data);
  },
  GET_BARCODE_LIST(state, data) {
    state.barcodeList = [];
    state.barcodeList = Object.freeze(data);
  },
  GET_PROVIDER_LIST(state, data) {
    state.providerList = data;
  },
  SCAN_BARCODE(state, barcode) {
    state.scanedBarcode = barcode;
  },
  CURRENT_PRODUCT_ADD(state, product) {
    state.currentProducts.push(product);
  },
  CURRENT_PRODUCT_EDIT(state, data) {
    let index =
      data.index || data.index === 0
        ? data.index
        : state.currentProducts.findIndex(x => x.id === data.id);
    Vue.set(state.currentProducts[index], data.field, data.value);
  },
  CURRENT_PRODUCT_REMOVE(state, index) {
    state.currentProducts.splice(index, 1);
  },
  CURRENT_PRODUCT_CLEAR(state) {
    state.currentProducts = [];
  },

  CLEAR_BUYER_INFO(state) {
    state.buyerInfo = {};
    state.cashLimit = {};
  },
  GET_BUYER_INFO(state, info) {
    state.buyerInfo = info.data;
    state.cashLimit = info.data?.bans?.cash_limit;
    state.banProducts = info.data?.bans?.ban_products;
    state.banCategories = info.data?.bans?.ban_categories;
  },
  GET_OUTLETS_LIST(state, data) {
    state.outletsList = data;
  },
  GET_USER_OUTLETS_LIST(state, data) {
    state.userOutletsList = data;
  },

  //---------------
  // Kitchen
  //---------------
  GET_KITCHEN_LIST(state, data) {
    state.kitchenList = data;
  },
  GET_KITCHEN_INFO(state, data) {
    state.kitchenInfo = data;
  },
  GET_KITCHEN_PRODUCTS(state, data) {
    state.kitchenProduct = [];
    Object.keys(data).forEach(item => {
      state.kitchenProducts.push(data[item]);
    });
  },
  GET_KITCHEN_CATEGORIES(state, data) {
    state.kitchenCategories = Object.freeze(data);
  },
  GET_KITCHEN_RECIPES(state, data) {
    state.kitchenRecipes = data;
  },
  GET_KITCHEN_ACCEPTANCE(state, data) {
    state.kitchenAcceptance = data;
  },
  GET_KITCHEN_WRITEOFFS(state, data) {
    state.kitchenWriteOffs = data;
  },
  GET_KITCHEN_PAGINATION(state, data) {
    state.currentPagination = data;
  },
  GET_CURRENT_PAGINATION(state, data) {
    state.currentPagination = data;
  },
  SET_CURRENT_KITCHEN(state, data) {
    state.currentKitchen = parseInt(data);
  },

  GET_CURRENT_SHOP_CATEGORIES(state, data) {
    state.currentShopCategories = data;
  },
  GET_CURRENT_SHOP_PRODUCTS(state, data) {
    state.currentShopProducts = data;
  },
};
