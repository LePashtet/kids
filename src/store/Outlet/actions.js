import axios from 'axios';
import { RepositoryFactory } from '@/helpers/apiFactory/RepositoryFactory';

const OutletRepository = RepositoryFactory.get('outlet');
const CommonRepository = RepositoryFactory.get('common');
import { storeErrorHandler } from '@/helpers/methods/errorHandler';

export default {
  async getProductStatuses({ commit, dispatch }) {
    try {
      const resp = await OutletRepository.getProductsStatuses();
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при получении списка статусов.',
          'Возникла ошибка  при получении списка статусов:'
        );
      } else {
        commit('GET_PRODUCT_STATUSES', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },
  getOutletId({ commit, rootState }, userID) {
    return axios({
      url:
        rootState.common.urls.outlet + '/rest/v1/offlineoutlet/getoutletbyuser',
      params: userID,
      method: 'GET',
    })
      .then(function(data) {
        commit('GET_OUTLET_ID', data);
      })
      .catch(function(err) {
        console.error(err);
      });
  },
  getOutletInfo({ commit, rootState }, id) {
    return axios({
      url: rootState.common.urls.outlet + '/rest/v1/offlineoutlet/getoutlet',
      params: id,
      method: 'GET',
    })
      .then(function(data) {
        commit('GET_OUTLET_INFO', data);
      })
      .catch(err => {
        console.error(err);
      });
  },
  getCategoriesList({ commit, rootState }, param) {
    axios({
      url: rootState.common.urls.outlet + '/rest/v1/offlinecategory/gettree',
      params: param,
      method: 'GET',
    })
      .then(data => {
        commit('GET_CATEGORY_LIST', data.data.data);
      })
      .catch(error => {
        console.error(error);
      });
  },

  getProductsList({ commit, rootState }, param) {
    axios({
      url:
        rootState.common.urls.outlet + '/rest/v1/offlineproduct/findproducts',
      params: param,
      method: 'GET',
    })
      .then(data => {
        commit('GET_PRODUCTS_LIST', data.data.data);
      })
      .catch(error => {
        console.error(error);
      });
  },
  getBarcodeList({ commit, rootState }, param) {
    axios({
      url: rootState.common.urls.outlet + '/rest/v1/offlineproduct/getbarcodes',
      params: param,
      method: 'GET',
    })
      .then(data => {
        commit('GET_BARCODE_LIST', data.data.data);
      })
      .catch(error => {
        console.error(error);
      });
  },
  async getProviderList({ commit, dispatch }, params) {
    try {
      const resp = await OutletRepository.getProvidersList(params);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка получения документов списания.',
          'Возникла ошибка при получении списка документов списания.'
        );
      } else {
        commit('GET_PROVIDER_LIST', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },
  scanBarcode({ commit }, barcode) {
    commit('SCAN_BARCODE', barcode);
  },
  addCurrentProduct({ commit }, product) {
    commit('CURRENT_PRODUCT_ADD', product);
  },
  editCurrentProduct({ commit }, data) {
    commit('CURRENT_PRODUCT_EDIT', data);
  },
  removeCurrentProduct({ commit }, index) {
    commit('CURRENT_PRODUCT_REMOVE', index);
  },
  clearCurrentProduct({ commit }) {
    commit('CURRENT_PRODUCT_CLEAR');
  },

  clearBuyerInfo({ commit }) {
    commit('CLEAR_BUYER_INFO');
  },
  async getBuyerInfo({ commit }, params) {
    try {
      const resp = await CommonRepository.getUserByAccount(params);
      commit('GET_BUYER_INFO', resp.data);
    } catch (err) {
      console.error(err);
    }
  },
  async getOutletsList({ commit }, params) {
    try {
      const resp = await OutletRepository.getOutletsList(params);
      commit('GET_OUTLETS_LIST', resp.data.data);
    } catch (err) {
      console.error(err);
    }
  },
  async getUserOutletsList({ commit }, params) {
    try {
      const resp = await OutletRepository.getUserOutletsList(params);
      commit('GET_USER_OUTLETS_LIST', resp.data.data);
    } catch (err) {
      console.error(err);
    }
  },

  //----------------
  // PROVIDER
  //----------------

  async getProvidersList({ commit, dispatch }, params) {
    try {
      const resp = await OutletRepository.getProvidersList(params);
      if (!resp.data || resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка получения поставщиков',
          'Возникла ошибка при получении поставщиков текущей торговой точки.'
        );
      } else {
        commit('GET_PROVIDER_LIST', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },

  //----------------
  // KITCHEN
  //----------------

  /**
   * Get kitchen list in the camp
   * @param { number } camp_id - current camp ID
   * @returns { Array } - Objects in contains camp_id, comment, created_at, id, name, updated_at
   */
  async getKitchenList({ commit }, params) {
    try {
      const resp = await OutletRepository.getKitchenList(params);
      commit('GET_KITCHEN_LIST', resp.data.data);
    } catch (err) {
      console.error(err);
    }
  },

  /**
   * Get kitchen Info
   * @param { number } kitchen_id - current kitchen ID
   * @returns { Object } - Object contains camp_id, comment, created_at, id, name, updated_at
   */
  async getKitchenInfo({ commit }, params) {
    try {
      const resp = await OutletRepository.getKitchenInfo(params);
      commit('GET_KITCHEN_INFO', resp.data.data);
    } catch (err) {
      console.error(err);
    }
  },

  /**
   * Get products in the Kitchen
   * @param { number } kitchen_id - current kitchen ID
   * @returns { Array } - Array of objects contains category_id, comment, created_at, id ,kitchen_id, name, price, qty, recipe, status, updated_at, vendor_code
   */
  async getKitchenProducts({ commit }, params) {
    try {
      const resp = await OutletRepository.getKitchenProducts(params);
      commit('GET_KITCHEN_PRODUCTS', resp.data.data);
    } catch (err) {
      console.error(err);
    }
  },

  /**
   * Get categories in the Kitchen
   * @param { number } kitchen_id - current kitchen ID
   * @returns { Object } - Object which contains categories hierarchy
   */
  async getKitchenCategories({ commit }, params) {
    try {
      const resp = await OutletRepository.getKitchenCategories(params);
      commit('GET_KITCHEN_CATEGORIES', resp.data.data);
    } catch (err) {
      console.error(err);
    }
  },
  /**
   * Get shop categories for selected shop in the kitchen
   * @param { number } outlet_id - selected_shop_ID
   * @returns { Object } - Object which contains categories hierarchy
   */
  async getKitchenRecipes({ commit, dispatch }, params) {
    try {
      const resp = await OutletRepository.getKitchenRecipes(params);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка получения рецептов',
          'Возникла ошибка при получении списка рецептов.'
        );
      } else {
        commit('GET_KITCHEN_RECIPES', resp.data.data);
        commit('GET_KITCHEN_PAGINATION', resp.data.pagination);
      }
    } catch (err) {
      console.error(err);
    }
  },
  /**
   * Get shop categories for selected shop in the kitchen
   * @param { number } kitchen_id  - selected_kitchen_ID
   * @returns { Object } - Object which contains categories hierarchy
   */
  async getKitchenAcceptance({ commit, dispatch }, params) {
    try {
      const resp = await OutletRepository.getKitchenAcceptance(params);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка получения документов приема товара.',
          'Возникла ошибка при получении списка документов приема товара.'
        );
      } else {
        commit('GET_KITCHEN_ACCEPTANCE', resp.data.data);
        commit('GET_KITCHEN_PAGINATION', resp.data.pagination);
      }
    } catch (err) {
      console.error(err);
    }
  },
  /**
   * Get shop categories for selected shop in the kitchen
   * @param { number } kitchen_id  - selected_kitchen_ID
   * @returns { Object } - Object which contains categories hierarchy
   */
  async getKitchenWriteOffs({ commit, dispatch }, params) {
    try {
      const resp = await OutletRepository.getKitchenWriteOffs(params);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка получения документов списания.',
          'Возникла ошибка при получении списка документов списания.'
        );
      } else {
        commit('GET_KITCHEN_WRITEOFFS', resp.data.data);
        commit('GET_KITCHEN_PAGINATION', resp.data.pagination);
      }
    } catch (err) {
      console.error(err);
    }
  },

  /**
   * Get shop categories for selected shop in the kitchen
   * @param { number } outlet_id - selected_shop_ID
   * @returns { Object } - Object which contains categories hierarchy
   */
  async getCurrentShopCategories({ commit, dispatch }, params) {
    try {
      const resp = await OutletRepository.getCurrentShopCategories(params);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка получения каталога',
          'Возникла ошибка при получении каталога.'
        );
      } else {
        commit('GET_CURRENT_SHOP_CATEGORIES', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },

  /**
   * Get shop products for selected shop in the kitchen
   * @param { number } outlet_id - selected_shop_ID
   * @returns { Array } - Array with products objects
   */
  async getCurrentShopProducts({ commit, dispatch }, params) {
    try {
      const resp = await OutletRepository.getCurrentShopProducts(params);
      if (!resp.data || resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка получения продуктов',
          'Возникла ошибка при получении продуктов текущей торговой точки.'
        );
      } else {
        commit('GET_CURRENT_SHOP_PRODUCTS', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },
};
