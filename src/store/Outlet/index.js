import actions from './actions';
import mutations from './mutations';
import getters from './getters';

function resetStore() {
  return {
    outletInfo: {},
    productStatuses: [],
    productCategories: [],
    productList: [],
    currentProducts: [],
    buyerInfo: {},
    cashLimit: {},
    banProducts: [],
    banCategories: [],
    barcodeList: [],
    providerList: [], //Поставщики
    scanedBarcode: null,
    outlet_id: localStorage.getItem('outletID') || '',
    outletsList: [],
    userOutletsList: [],

    // KITCHEN
    kitchenList: [],
    kitchenInfo: {},
    kitchenProducts: [],
    kitchenCategories: {},
    kitchenRecipes: [],
    kitchenAcceptance: [],
    kitchenWriteOffs: [],
    currentPagination: {},
    currentKitchen: null,
    currentShopCategories: {},
    currentShopProducts: [],
  };
}

export default {
  namespaced: true,
  state: resetStore(),
  actions: actions,
  getters: getters,
  mutations: {
    resetStore(state) {
      Object.assign(state, resetStore());
    },
    ...mutations,
  },
};
