import Vue from 'vue';
import Vuex from 'vuex';
import Account from './Account/index';
import Outlet from './Outlet/index';
import Common from './Common/index';
import User from './User/index';
import Calendar from './Calendar/index';

import Notifications from './Notifications/index';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    account: Account,
    outlet: Outlet,
    common: Common,
    user: User,
    notifications: Notifications,
    calendar: Calendar,
  },
});
