import Vue from 'vue';
export default {
  ADD_NEW_NOTIFICATION(state, notification) {
    let id = state.notificationList.length + 1;
    notification.id = id;
    state.notificationList.push(notification);
  },
  CHANGE_NOTIFICATION_STATUS(state, data) {
    Vue.set(
      state.notificationList[
        state.notificationList.findIndex(x => x.id == data.id)
      ],
      'status',
      data.status
    );
  },
};
