import actions from './actions';
import mutations from './mutations';

export default {
  namespaced: true,
  state: {
    // @todo Поменять хардкод на ответ с бэка
    // notificationType: '', success, warning, info, error
    // status: new, shown,
    notificationList: [],
  },
  actions: actions,
  mutations: mutations,
};
