import { format } from 'date-fns';

export default {
  addNewNotification({ commit }, notification) {
    notification.time = format(new Date(), 'hh:mm');
    notification.date = format(new Date(), 'dd.MM.yyyy');
    commit('ADD_NEW_NOTIFICATION', notification);
  },
  changeNotificationStatus({ commit }, data) {
    commit('CHANGE_NOTIFICATION_STATUS', data);
  },
};
