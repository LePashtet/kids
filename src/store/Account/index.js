import actions from './actions';
import mutations from './mutations';
import getters from './getters';

export default {
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    userId: localStorage.getItem('userId') || '',
    role: localStorage.getItem('role') || '',
    // outlet_id: localStorage.getItem('outletID') || '',
    userInfo: {},
  },
  actions: actions,
  getters: getters,
  mutations: mutations,
};
