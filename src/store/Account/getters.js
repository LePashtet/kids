export default {
  isLoggedIn: state => !!state.token,
  authStatus: state => state.status,
  authRole: state => state.role,
  userId: state => state.userId,
};
