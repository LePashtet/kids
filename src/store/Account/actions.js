import axios from 'axios';
import { asyncSetLocalStorage } from '@/helpers/methods/asyncLocalStorage';
import { storeErrorHandler } from '@/helpers/methods/errorHandler';
import { RepositoryFactory } from '@/helpers/apiFactory/RepositoryFactory';

const UserRepository = RepositoryFactory.get('user');
// import Vue from 'vue'

export default {
  login({ commit, rootState, dispatch }, user) {
    return new Promise((resolve, reject) => {
      commit('auth_request');
      axios({
        url: rootState.common.urls.core + '/rest/v1/auth/login',
        data: user,
        method: 'POST',
      }).then(resp => {
        if (resp.data.status != 'ok') {
          storeErrorHandler(
            dispatch,
            resp,
            'Ошибка авторизации.',
            'Возникла ошибка при авторизации.'
          );
          reject();
        } else {
          const token = resp.data.authKey;
          const userId = resp.data.data.userId;
          const role = resp.data.data.role;
          asyncSetLocalStorage('token', token);
          asyncSetLocalStorage('userId', userId);
          asyncSetLocalStorage('role', role);
          commit('auth_success', {
            token: token,
            userId: userId,
            role: role,
          });
          resolve(resp);
        }
      });
    });
  },
  register({ commit, rootState, dispatch }, user) {
    return new Promise((resolve, reject) => {
      commit('auth_request');
      axios({
        url: rootState.common.urls.core + '/rest/v1/auth/registration',
        data: user,
        method: 'POST',
      })
        .then(resp => {
          if (resp.data.error) {
            let notification = {};
            notification.title = 'Регистрация.';
            notification.text = 'Ошибка регистрации. ';
            notification.notificationType = 'error';
            notification.status = 'new';
            dispatch('notifications/addNewNotification', notification, {
              root: true,
            });
          } else {
            const token = resp.data.authKey;
            const userId = resp.data.userId;
            const role = resp.data.role;
            localStorage.setItem('token', token);
            localStorage.setItem('userId', userId);
            localStorage.setItem('role', role);
            // Add the following line:
            // axios.defaults.headers.common['Authorization'] = token
            commit('auth_success', {
              token: token,
              userId: userId,
              role: role,
            });
            resolve(resp);
          }
        })
        .catch(err => {
          commit('auth_error', err);
          localStorage.removeItem('token');
          reject(err);
        });
    });
  },
  logout({ commit }) {
    return new Promise((resolve, reject) => {
      commit('logout');
      commit('outlet/resetStore', { root: true });
      commit('user/resetStore', { root: true });
      localStorage.removeItem('outletID');
      localStorage.removeItem('token');
      localStorage.removeItem('userId');
      localStorage.removeItem('role');
      resolve();
      reject();
    });
  },
  // getOutletId({ commit, rootState }, userID) {
  //   return axios({
  //     url:
  //       rootState.common.urls.outlet + '/rest/v1/offlineoutlet/getoutletbyuser',
  //     params: userID,
  //     method: 'GET',
  //   })
  //     .then(function(data) {
  //       commit('GET_OUTLET_ID', data);
  //     })
  //     .catch(function(err) {
  //       console.log(err);
  //     });
  // },
  async getAccountInfo({ commit }, payload) {
    try {
      const resp = await UserRepository.getAccountInfo(payload);
      console.log(resp);
    } catch (e) {
      console.log(e);
    }
  },
  async getAccountLogs({ commit }, payload) {
    try {
      const resp = await UserRepository.getAccountLogs(payload);
      console.log(resp);
    } catch (e) {
      console.log(e);
    }
  },
};
