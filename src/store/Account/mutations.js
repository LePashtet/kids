export default {
  auth_request(state) {
    state.status = 'loading';
  },





  auth_success(state, data) {
    state.status = 'success';
    state.token = data.token;
    state.userId = data.userId;
    state.role = data.role;
  },
  auth_error(state) {
    state.status = 'error';
  },
  logout(state) {
    state.status = null;
    state.token = null;
    state.userId = null;
    state.role = null;
  },
  // GET_OUTLET_ID(state, data){
  //     // state.outlet_id = data.data.data.id;
  //     if (localStorage.getItem('outletID') != data.data.data.id) {
  //         localStorage.setItem('outletID', data.data.data.id)
  //     }
  // },
};
