import axios from 'axios';
import { storeErrorHandler } from '@/helpers/methods/errorHandler';
import { RepositoryFactory } from '@/helpers/apiFactory/RepositoryFactory';

const UserRepository = RepositoryFactory.get('user');
const OutletRepository = RepositoryFactory.get('outlet');

export default {
  async getUserInfo({ commit, dispatch }, user) {
    try {
      const resp = await UserRepository.getUserInfo(user);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при получении списка статусов.',
          'Возникла ошибка  при получении списка статусов:'
        );
      } else {
        commit('SET_USER_INFO', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },

  ///////////////KIDS////////////////////

  async getUsersKids({ commit, dispatch }, data) {
    try {
      const resp = await UserRepository.getAssignedKids(data);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при получении данных детей.',
          'Возникла ошибка  при получении данных детей:'
        );
      } else {
        commit('SET_USER_KIDS', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },

  async getMyKidsInfo({ commit, dispatch }, user) {
    try {
      const resp = await UserRepository.getUserInfo(user);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при получении данных детей.',
          'Возникла ошибка  при получении данных детей:'
        );
      } else {
        commit('SET_USER_KIDS_INFO', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },

  //////////////////////////////////////////
  //Deprecated
  /////////////////////////////////////////

  async getUserParents({ commit, dispatch }) {
    try {
      const resp = await UserRepository.getLinkedParents({
        user_id: localStorage.getItem('userId'),
      });
      if (resp.data?.status === 'ok') {
        commit('SET_USER_PARENTS', resp.data?.data);
      } else {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка получения данных',
          'Мы не смогли получить ваших родителей, попробуйте перезагрузить страницу'
        );
      }
    } catch (err) {
      storeErrorHandler(
        dispatch,
        'Something gonna wrong',
        'Ошибка получения данных',
        'Мы не смогли получить ваших родителей, попробуйте перезагрузить страницу'
      );
    }
  },

  async getCampInfo({ commit, dispatch }, camp) {
    try {
      const resp = await UserRepository.getCampInfo(camp);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при получении информации о лагере.',
          'Возникла ошибка  при получении информации о лагере:'
        );
      } else {
        commit('GET_CAMP_INFO', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },
  async getSquadInfo({ commit, dispatch }, squad) {
    try {
      const resp = await UserRepository.getSquadInfo(squad);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при получении информации об отряде.',
          'Возникла ошибка  при получении информации об отряде:'
        );
      } else {
        commit('GET_SQUAD_INFO', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },
  async getKidAccount({ commit, dispatch }, user) {
    try {
      const resp = await UserRepository.getKidAccount(user);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при получении информации о ребенке.',
          'Возникла ошибка  при информации о ребенке:'
        );
      } else {
        commit('GET_KID_ACCOUNT', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },
  async getKidAccountLogs({ commit, dispatch }, user) {
    try {
      const resp = await UserRepository.getKidAccountLogs(user);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при получении информации по счету.',
          'Возникла ошибка  при информации по счету:'
        );
      } else {
        console.log(resp);
        commit('GET_KID_ACCOUNT_LOGS', resp.data.data.account.accountLogs);
      }
    } catch (err) {
      console.error(err);
    }
  },
  async getKidBills({ commit, dispatch }, user) {
    try {
      const resp = await UserRepository.getKidBills(user);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при получении информации по счету.',
          'Возникла ошибка  при информации по счету:'
        );
      } else {
        console.log(resp);
        commit('GET_KID_BILLS', resp.data.data.checks);
      }
    } catch (err) {
      console.error(err);
    }
  },
  async findKid({ commit, dispatch }, data) {
    try {
      const resp = await UserRepository.findKid(data);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при поиске ребенка.',
          'Возникла ошибка  при поиске ребенка:'
        );
      } else {
        commit('FIND_KID', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },
  async findUser({ commit, dispatch }, data) {
    try {
      const resp = await UserRepository.findUser(data);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при поиске пользователя.',
          'Возникла ошибка  при поиске пользователя:'
        );
      } else {
        commit('SET_AUTHORITY', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },
  async getOutletList({ commit, dispatch }, params) {
    try {
      const resp = await OutletRepository.getOutletsList(params);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при получении данных детей.',
          'Возникла ошибка  при получении данных детей:'
        );
      } else {
        commit('GET_OUTLET_LIST', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },
  async getOutletProducts({ commit, dispatch }, params) {
    try {
      const resp = await OutletRepository.getCurrentShopProducts({
        outlet_id: params.id,
      });
      if (!resp.data || resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка получения продуктов',
          'Возникла ошибка при получении продуктов текущей торговой точки.'
        );
      } else {
        let obj = {
          resp: resp.data.data,
          i: params.i,
        };
        commit('GET_OUTLET_PRODUCTS', obj);
      }
    } catch (err) {
      console.error(err);
    }
  },
  async getOutletCategories({ commit, dispatch }, params) {
    try {
      const resp = await OutletRepository.getCurrentShopCategories({
        outlet_id: params.id,
      });
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка получения каталога',
          'Возникла ошибка при получении каталога.'
        );
      } else {
        let obj = {
          resp: resp.data.data,
          i: params.i,
        };
        commit('GET_OUTLET_CATEGORIES', obj);
      }
    } catch (err) {
      console.error(err);
    }
  },
  async getProductRestricts({ commit, dispatch }, params) {
    try {
      const resp = await OutletRepository.getProductRestricts(params);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при получении ограничений по продуктам.',
          'Возникла ошибка  при получении ограничений по продуктам:'
        );
      } else {
        commit('GET_PRODUCT_RESTRICTS', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }
  },
  getCategoryRestricts({ commit, rootState }, params) {
    return axios({
      url:
        rootState.common.urls.outlet +
        '/rest/v1/offlinecategory/getbancategories',
      params: params,
      method: 'GET',
    })
      .then(function(resp) {
        commit('GET_CATEGORY_RESTRICTS', resp);
      })
      .catch(function() {
        console.log('getMyKidsList FAILURE!!');
      });
  },
  async getCashLimit({ commit, dispatch }, params) {
    try {
      const resp = await OutletRepository.getCashLimit(params);
      if (resp.data.status != 'ok') {
        storeErrorHandler(
          dispatch,
          resp,
          'Ошибка при получении ограничений по продуктам.',
          'Возникла ошибка  при получении ограничений по продуктам:'
        );
      } else {
        commit('GET_CASH_LIMIT', resp.data.data);
      }
    } catch (err) {
      console.error(err);
    }

    // return axios({
    //   url:
    //     rootState.common.urls.outlet + '/rest/v1/offlinepayment/getcashlimit',
    //   params: params,
    //   method: 'GET',
    // })
    //   .then(function(resp) {
    //     commit('GET_CASH_LIMIT', resp);
    //   })
    //   .catch(function() {
    //     console.log('getCashLimit FAILURE!!');
    //   });
  },
};
