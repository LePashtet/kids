import actions from './actions';
import mutations from './mutations';
import getters from './getters';

function resetStore() {
  return {
    info: {},
    departments: [],
    roles: [],

    /////////////PARENTS///////////

    kids: [],
    tempAuthority: [],
    activeKid: {},
    activeDepartment: {},
    kidsInfo: [],
    kidsDepartments: [],
    currentUserParents: [],
    roleInfo: {},

    //////////////////////////////////////////
    //Deprecated
    /////////////////////////////////////////
    // currentUser: {},
    // currentUserDepartment: [],
    // currentUserRole: [],
    // kidBills: [],
    // kidAccountOperations: [],
    // tempKid: {},
    // assignedKids: [],
    // myKidsList: [],
    // activeKid: {},
    // campInfo: {},
    // squadInfo: {},
    // activeKidCamp: {},
    // outletsList: [],
    // outletsTemp: [],
    // outletsInfo: [],
    // productsRestricts: [],
    // categoryRestricts: [],
    // cashLimit: {},
  };
}

export default {
  namespaced: true,
  state: resetStore(),
  actions: actions,
  getters: getters,
  mutations: {
    resetStore(state) {
      Object.assign(state, resetStore());
    },
    ...mutations,
  },
};
