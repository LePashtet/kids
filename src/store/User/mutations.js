import Vue from 'vue';
export default {
  SET_USER_INFO(state, data) {
    state.info = data;
    state.departments = data.departmentClient;
    state.roles = [...data.groupUser, ...data.departmentUser];
  },
  ///////////////PARENTS///////////////////

  SET_USER_KIDS(state, info) {
    state.kids = info;
  },
  CLEAR_USER_KIDS_INFO(state) {
    state.kidsInfo = [];
  },
  SET_USER_KIDS_INFO(state, info) {
    let departments = {
      user_id: info.id,
    };
    if (info.departmentClient.length > 0) {
      info.departmentClient.map(item => {
        if (!departments[item.type_department]) {
          departments[item.type_department] = [];
        }
        departments[item.type_department].push(item.department_id);
      });
    }
    state.kidsDepartments.push(departments);
    state.kidsInfo.push(info);
  },
  SET_ACTIVE_DEPARTMENT(state, data) {
    state.activeDepartment = data[0];
  },
  SET_USER_PARENTS(state, data) {
    state.currentUserParents = data;
  },
  SET_ROLE_INFO(state, data) {
    state.roleInfo = data;
  },

  ///////////////KIDS//////////////////////

  //////////////////////////////////////////
  //Deprecated
  /////////////////////////////////////////

  SET_ACTIVE_KID(state, data) {
    state.activeKid = data;
  },

  GET_KID_ACCOUNT(state, info) {
    state.activeKidCamp = info.user;
  },
  GET_KID_BILLS(state, info) {
    state.kidBills = info;
  },
  GET_KID_ACCOUNT_LOGS(state, info) {
    state.kidAccountOperations = info;
  },
  FIND_KID(state, kid) {
    state.tempKid = kid;
  },
  CLEAR_TEMP_KID(state) {
    state.tempKid = {};
  },
  GET_CAMP_INFO(state, camp) {
    state.campInfo = camp;
  },
  GET_SQUAD_INFO(state, squad) {
    state.squadInfo = squad;
  },
  SET_AUTHORITY(state, data) {
    state.tempAuthority = data;
  },
  CLEAR_AUTHORITY(state) {
    state.tempAuthority = [];
  },

  GET_OUTLET_LIST(state, info) {
    state.outletsList = info;
  },

  CLEAR_OUTLET_INFO(state) {
    state.outletsInfo = [];
  },
  GET_OUTLET_PRODUCTS(state, data) {
    state.outletsTemp[data.i] = {};
    Vue.set(state.outletsTemp[data.i], 'products', data.resp);
  },
  GET_OUTLET_CATEGORIES(state, data) {
    Vue.set(state.outletsTemp[data.i], 'categories', data.resp);
    state.outletsInfo.push(state.outletsTemp[data.i]);
  },
  GET_PRODUCT_RESTRICTS(state, data) {
    state.productsRestricts = [];
    state.productsRestricts = data;
  },
  GET_CATEGORY_RESTRICTS(state, data) {
    state.categoryRestricts = [];
    state.categoryRestricts = data.data.data;
  },
  GET_CASH_LIMIT(state, data) {
    state.cashLimit = {};
    state.cashLimit = data;
  },
};
