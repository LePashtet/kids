import Vue from 'vue';
import Router from 'vue-router';

const Main = () => import('@/views/MainPage');
const Home = () => import('@/views/Home');
const About = () => import('../views/About.vue');
const Partner = () => import('@/views/Partner');
const Camp = () => import('@/views/partner/Camp');
const Rates = () => import('@/views/Rates');

const School = () => import('@/views/partner/School');
const Kindergarten = () => import('@/views/partner/Kindergarten');
// const authTest = () => import('@/views/!deleteauthTest');
const Contacts = () => import('@/views/Contacts');
const SecurityPage = () => import('@/views/Security');
const BrandCenter = () => import('@/views/BrandCenter');
const RegisterParentKid = () => import('@/views/home/auth/RegisterParentKid');
const EmailConfirm = () => import('@/views/home/auth/EmailConfirm');
const Register = () => import('@/views/home/auth/Register');
const RegisterObject = () => import('@/views/home/auth/RegisterObject');

import store from './../store/store';

// import userEdit from './../views/user/edit'
// import Old from './routes/old'
// import Auth from './routes/auth';
import Shop from './routes/shop';
import Kitchen from './routes/kitchen';
import Parents from './routes/parents';
import Kids from './routes/kids';
import Security from './routes/security';
import Leader from './routes/leader';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  linkExactActiveClass: '-active',
  routes: [
    {
      path: '/',
      component: Main,
      children: [
        {
          path: '',
          name: 'home',
          component: Home,
          meta: {
            toTop: true,
          },
        },
        {
          path: '/about',
          name: 'about',
          component: About,
          meta: {
            toTop: true,
          },
        },
        {
          path: '/brand-center',
          name: 'brand-center',
          component: BrandCenter,
          meta: {
            toTop: true,
          },
        },
        {
          path: '/partner',
          component: Partner,
          children: [
            {
              path: '',
              name: 'kindergarten',
              component: Kindergarten,
              meta: {
                toTop: true,
              },
            },
            {
              path: 'school',
              name: 'school',
              component: School,
              meta: {
                toTop: true,
              },
            },
            {
              path: 'camp',
              name: 'camp',
              component: Camp,
              meta: {
                toTop: true,
              },
            },
          ],
        },
        {
          path: '/rates',
          name: 'rates',
          component: Rates,
          meta: {
            toTop: true,
          },
        },
        // {
        //   path: '/auth-test',
        //   name: 'auth-test',
        //   component: authTest,
        // },
        {
          path: '/contacts',
          name: 'contacts',
          component: Contacts,
          meta: {
            toTop: true,
          },
        },
        {
          path: '/security',
          name: 'securityPage',
          component: SecurityPage,
          meta: {
            toTop: true,
          },
        },
        {
          path: '/register/:kid',
          name: 'register-parent-kid',
          component: RegisterParentKid,
          meta: {
            toTop: true,
          },
        },
        {
          path: '/register-user',
          name: 'register-user',
          component: Register,
          meta: {
            toTop: true,
          },
        },
        {
          path: '/register-company',
          name: 'register-company',
          component: RegisterObject,
          meta: {
            toTop: true,
          },
        },
        {
          path: 'email-confirm',
          name: 'email-confirm',
          component: EmailConfirm,
          props: { token: true },
        },
      ],
    },
    // Old,
    // Auth,
    Shop,
    Kitchen,
    Parents,
    Kids,
    Security,
    Leader,
    {
      path: '*',
      name: '404',
      component: () => import('./../views/404.vue'),
    },
  ],
  scrollBehavior(to) {
    // eslint-disable-line
    if (to.matched.some(record => record.meta.toTop)) {
      return { x: 0, y: 0 };
    }
  },
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next();
      return;
    }
    next({ name: 'home' });
  } else if (to.matched.some(record => record.meta.requiresOutletUser)) {
    if (
      localStorage.getItem('role') === 'outlet_user' ||
      localStorage.getItem('role') === 'outlet_manager'
    ) {
      next();
      return;
    }
    next({ name: 'home' });
  } else if (to.matched.some(record => record.meta.requiresOutletAdmin)) {
    if (localStorage.getItem('role') === 'outlet_manager') {
      next();
      return;
    } else if (localStorage.getItem('role') === 'outlet_user') {
      next('/shop/');
      return;
    }
    next({ name: 'home' });
  } else if (to.matched.some(record => record.meta.requiresParent)) {
    if (localStorage.getItem('role') === 'parent') {
      next();
      return;
    }
    next({ name: 'home' });
  } else if (to.matched.some(record => record.meta.requiresChild)) {
    if (localStorage.getItem('role') === 'child') {
      next();
      return;
    }
    next({ name: 'home' });
  } else if (to.matched.some(record => record.meta.requiresSecurity)) {
    if (localStorage.getItem('role') === 'security') {
      next();
      return;
    }
    next({ name: 'home' });
  } else if (to.matched.some(record => record.meta.requiresLeader)) {
    if (localStorage.getItem('role') === 'leader') {
      next();
      return;
    }
    next({ name: 'home' });
  } else {
    next();
  }
});

export default router;
