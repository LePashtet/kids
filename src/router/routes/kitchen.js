const Kitchen = () => import('@/views/Kitchen');
const Acceptance = () => import('@/views/shop/KitchenAcceptance');
const WriteOff = () => import('@/views/shop/KitchenWriteOff');
const KitchenRecipes = () => import('@/views/shop/KitchenRecipes');
const KitchenAcceptanceList = () =>
  import('@/views/shop/KitchenAcceptanceList');
const KitchenAcceptanceListItem = () =>
  import('@/views/shop/KitchenAcceptanceListItem.vue');
const KitchenWriteOffList = () => import('@/views/shop/KitchenWriteOffList');
const KitchenWriteOffListItem = () =>
  import('@/views/shop/KitchenWriteOffListItem');

export default {
  path: '/kitchen/',
  // name: 'shop',
  component: Kitchen,
  children: [
    {
      path: '',
      name: 'cooking',
      component: KitchenRecipes,
      meta: {
        // requiresOutletAdmin: true
        requiresOutletUser: true,
      },
    },
    {
      path: 'acceptance/',
      name: 'kitchenAcceptance',
      component: Acceptance,
      meta: {
        // requiresOutletAdmin: true
        requiresOutletUser: true,
      },
    },
    {
      path: 'acceptance-list/',
      name: 'kitchenAcceptanceList',
      component: KitchenAcceptanceList,
      meta: {
        // requiresOutletAdmin: true
        requiresOutletUser: true,
      },
    },
    {
      path: 'acceptance-list-item/:id',
      name: 'kitchenAcceptanceListItem',
      component: KitchenAcceptanceListItem,
      meta: {
        // requiresOutletAdmin: true,
        requiresOutletUser: true,
      },
    },
    {
      path: 'write-off/',
      name: 'kitchenWriteOff',
      component: WriteOff,
      meta: {
        // requiresOutletAdmin: true
        requiresOutletUser: true,
      },
    },
    {
      path: 'write-off-list/',
      name: 'kitchenWriteOffList',
      component: KitchenWriteOffList,
      meta: {
        // requiresOutletAdmin: true
        requiresOutletUser: true,
      },
    },
    {
      path: 'write-off-list-item/:id',
      name: 'kitchenWriteOffListItem',
      component: KitchenWriteOffListItem,
      meta: {
        // requiresOutletAdmin: true
        requiresOutletUser: true,
      },
    },
  ],
};
