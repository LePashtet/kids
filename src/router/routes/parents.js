const Parents = () => import('./../../views/Parents.vue');
const Main = () => import('../../views/user/parents/Main.vue');
const Files = () => import('@/views/user/parents/Files.vue');

const PaySuccess = () => import('../../views/user/parents/PaySuccess.vue');
const PayFall = () => import('../../views/user/parents/PayFall.vue');
const Notifications = () => import('@/views/user/common/NotificationHistory');
const ServiceMessages = () =>
  import('@/views/user/parents/KidsServiceMessages');
const KidsCalendar = () => import('@/views/user/parents/KidsCalendar');
const Calendar = () => import('@/views/user/parents/Calendar');

const KidsOnlineStore = () => import('@/views/user/parents/KidsOnlineStore');
const Authority = () => import('@/views/user/parents/Authority');
const KidsAccount = () => import('@/views/user/parents/KidsAccount');
const Account = () => import('@/views/user/parents/Account');
const Debt = () => import('@/views/user/kids/kindergarten/DebtsItem');

const KidsObject = () => import('@/views/user/parents/KidsObject');
const Settings = () => import('@/views/user/parents/Settings');
const QR = () => import('@/views/user/common/QR');
const RegisterKid = () => import('@/components/user/RegisterKid');
const CreateKid = () => import('@/views/user/parents/CreateKid');

const ParentRegisterObject = () =>
  import('@/components/user/ParentRegisterObject');

export default {
  path: '/parents/',
  component: Parents,
  meta: {
    requiresParent: true,
  },
  children: [
    {
      path: '',
      name: 'parent',
      component: Main,
      meta: {
        requiresParent: true,
        toTop: true,
      },
    },
    {
      path: 'documents',
      name: 'parents-files',
      component: Files,
      meta: {
        requiresChild: true,
      },
    },
    {
      path: 'pay-success/',
      name: 'pay-success',
      component: PaySuccess,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'pay-fall/',
      name: 'pay-fall',
      component: PayFall,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'notifications/',
      name: 'notifications',
      component: Notifications,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'service-messages/',
      name: 'service-messages',
      component: ServiceMessages,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'calendar',
      name: 'calendar',
      component: Calendar,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'kid-calendar',
      name: 'kidcalendar',
      component: KidsCalendar,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'online-store',
      name: 'online-store',
      component: KidsOnlineStore,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'authority',
      name: 'authority',
      component: Authority,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'kids-account',
      name: 'kids-account',
      component: KidsAccount,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'debt/:id',
      name: 'debt',
      component: Debt,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'account',
      name: 'account',
      component: Account,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'kids-object',
      name: 'kids-object',
      component: KidsObject,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'settings',
      name: 'settings',
      component: Settings,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'qr',
      name: 'qr',
      component: QR,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'register-kid/:kid',
      name: 'register-kid',
      component: RegisterKid,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'create-kid/',
      name: 'create-kid',
      component: CreateKid,
      meta: {
        requiresParent: true,
      },
    },
    {
      path: 'register-object',
      name: 'register-object',
      component: ParentRegisterObject,
      meta: {
        requiresParent: true,
        toTop: true,
      },
    },
  ],
};
