const Shop = () => import('./../../views/Shop.vue');
const Sale = () => import('./../../views/shop/ShopSale.vue');
const SaleList = () => import('./../../views/shop/ShopSaleList.vue');
const SaleListItem = () => import('./../../views/shop/ShopSaleListItem.vue');
const Acceptance = () => import('./../../views/shop/ShopAcceptance.vue');
const AcceptanceList = () =>
  import('./../../views/shop/ShopAcceptanceList.vue');
const AcceptanceListItem = () =>
  import('./../../views/shop/ShopAcceptanceListItem.vue');
const WriteOff = () => import('./../../views/shop/ShopWriteOff.vue');
const WriteOffList = () => import('./../../views/shop/ShopWriteOffList.vue');
const WriteOffListItem = () =>
  import('./../../views/shop/ShopWriteOffListItem.vue');
const Reserve = () => import('./../../views/shop/ShopReserve.vue');
const Return = () => import('./../../views/shop/ShopReturn.vue');
const Overcharge = () => import('./../../views/shop/ShopOvercharge.vue');
const OverchargeList = () =>
  import('./../../views/shop/ShopOverchargeList.vue');
const OverchargeListItem = () =>
  import('./../../views/shop/ShopOverchargeListItem.vue');
const ProductLog = () => import('./../../views/shop/ShopProductLog.vue');

export default {
  path: '/shop/',
  // name: 'shop',
  component: Shop,
  children: [
    {
      path: '',
      name: 'sale',
      component: Sale,
      meta: {
        // requiresOutletAdmin: true
        requiresOutletUser: true,
      },
    },
    {
      path: 'return/',
      name: 'return',
      component: Return,
      meta: {
        // requiresOutletAdmin: true
        requiresOutletUser: true,
      },
    },
    {
      path: 'product-log/:id',
      name: 'productLog',
      component: ProductLog,
      meta: {
        requiresOutletAdmin: true,
      },
    },
    {
      path: 'sale-list/',
      name: 'saleList',
      component: SaleList,
    },
    {
      path: 'sale-list-item/:id',
      name: 'saleListItem',
      component: SaleListItem,
    },
    {
      path: 'acceptance/',
      name: 'acceptance',
      component: Acceptance,
      meta: {
        requiresOutletAdmin: true,
      },
    },
    {
      path: 'acceptance-list/',
      name: 'acceptanceList',
      component: AcceptanceList,
      meta: {
        requiresOutletAdmin: true,
      },
    },
    {
      path: 'acceptance-list-item/:id',
      name: 'acceptanceListItem',
      component: AcceptanceListItem,
      meta: {
        requiresOutletAdmin: true,
      },
    },
    {
      path: 'write-off/',
      name: 'writeOff',
      component: WriteOff,
      meta: {
        requiresOutletAdmin: true,
      },
    },
    {
      path: 'write-off-list/',
      name: 'writeOffList',
      component: WriteOffList,
      meta: {
        requiresOutletAdmin: true,
      },
    },
    {
      path: 'write-off-list-item/:id',
      name: 'writeOffListItem',
      component: WriteOffListItem,
      meta: {
        requiresOutletAdmin: true,
      },
    },
    {
      path: 'reserve/',
      name: 'reserve',
      component: Reserve,
      meta: {
        // requiresOutletAdmin: true
        requiresOutletUser: true,
      },
    },
    {
      path: 'overcharge/',
      name: 'overcharge',
      component: Overcharge,
      meta: {
        requiresOutletAdmin: true,
      },
    },
    {
      path: 'overcharge-list/',
      name: 'overchargeList',
      component: OverchargeList,
      meta: {
        requiresOutletAdmin: true,
      },
    },
    {
      path: 'overcharge-list-item/:id',
      name: 'overchargeListItem',
      component: OverchargeListItem,
      meta: {
        requiresOutletAdmin: true,
      },
    },
  ],
};
