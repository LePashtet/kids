const SecurityHome = () => import('@/views/user/security/Main');
const SecurityMain = () => import('@/views/user/security/SecurityMain');
const SecurityDebtors = () => import('@/views/user/security/SecurityDebtors');
const Notifications = () => import('@/views/user/common/NotificationHistory');

export default {
  path: '/security-manager/',
  component: SecurityHome,
  children: [
    {
      path: '',
      name: 'security',
      component: SecurityMain,
      meta: {
        requiresSecurity: true,
      },
    },
    {
      path: 'notifications/',
      name: 'SecurityNotifications',
      component: Notifications,
      meta: {
        requiresSecurity: true,
      },
    },
    {
      path: 'debtors/',
      name: 'securityDebtors',
      component: SecurityDebtors,
      meta: {
        requiresSecurity: true,
      },
    },
  ],
};
