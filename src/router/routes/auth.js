import Router from 'vue-router';
import Shop from '@/router/routes/shop';
import Kitchen from '@/router/routes/kitchen';
import Parents from '@/router/routes/parents';
import Kids from '@/router/routes/kids';
import Security from '@/router/routes/security';
import Leader from '@/router/routes/leader';

const Auth = () => import('../../views/!deleteAuth.vue');
const Login = () => import('../../views/home/auth/Login.vue');
const Forgot = () => import('../../views/home/auth/Forgot.vue');
const Register = () => import('../../components/home/auth/RegisterParent.vue');
const Exit = () => import('./../../views/auth/Exit.vue');

export default {
  path: '/register-user',
  component: Main,
};
