const Leader = () => import('@/views/Leader');
const LeaderMain = () => import('@/views/user/leader/LeaderMain');
const LeaderList = () => import('@/views/user/leader/LeaderList');
const LeaderQr = () => import('@/views/user/leader/LeaderQr');
const Notifications = () => import('@/views/user/common/NotificationHistory');

export default {
  path: '/leader/',
  component: Leader,
  children: [
    {
      path: '',
      name: 'leader',
      component: LeaderMain,
      meta: {
        requiresLeader: true,
      },
    },
    {
      path: 'lists/',
      name: 'leaderList',
      component: LeaderList,
      meta: {
        requiresLeader: true,
      },
    },
    {
      path: 'notifications/',
      name: 'LeaderNotifications',
      component: Notifications,
      meta: {
        requiresLeader: true,
      },
    },
    {
      path: 'qr',
      name: 'leaderQr',
      component: LeaderQr,
      meta: {
        requiresLeader: true,
      },
    },
  ],
};
