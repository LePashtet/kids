const Kids = () => import('@/views/Kids.vue');
const Main = () => import('@/views/user/kids/Main.vue');
const Kindergarten = () => import('@/views/user/kids/Kindergarten');

/*Kindergarten*/
const Account = () => import('@/views/user/kids/Account.vue');
const Files = () => import('@/views/user/kids/Files.vue');
const KinderAccount = () => import('@/views/user/kids/kindergarten/Account');
const Debt = () => import('@/views/user/kids/kindergarten/DebtsItem');
const OnlineStore = () => import('@/views/user/kids/OnlineStore.vue');
const Object = () => import('@/views/user/kids/Object.vue');
const Settings = () => import('@/views/user/kids/Settings.vue');
const ServiceMessages = () => import('@/views/user/kids/ServiceMessages.vue');
const QR = () => import('@/views/user/common/QR');
const OfflineOutlet = () => import('@/components/user/account/OfflineOutlet');
const KidsChat = () => import('@/views/user/kids/Chat');
const KidsNotifications = () =>
  import('@/views/user/common/NotificationHistory');
const KidsCalendar = () => import('@/views/user/kids/KidCalendar');

export default {
  path: '/kids/',
  component: Kids,
  meta: {
    requiresChild: true,
  },
  children: [
    {
      path: '',
      name: 'kidsMain',
      component: Main,
      meta: {
        requiresChild: true,
        toTop: true,
      },
    },
    {
      path: 'account',
      name: 'kid-account',
      component: Account,
      meta: {
        requiresChild: true,
      },
    },
    {
      path: 'store',
      name: 'kid-store',
      component: OnlineStore,
      meta: {
        requiresChild: true,
      },
    },
    {
      path: 'documents',
      name: 'kid-files',
      component: Files,
      meta: {
        requiresChild: true,
      },
    },
    {
      path: 'settings',
      name: 'kid-settings',
      component: Settings,
      meta: {
        requiresChild: true,
      },
    },
    {
      path: 'service-messages',
      name: 'kids-service-messages',
      component: ServiceMessages,
      meta: {
        requiresChild: true,
      },
    },
    {
      path: 'offline-outlet',
      component: OfflineOutlet,
      meta: {
        requiresChild: true,
      },
    },
    {
      path: 'chat',
      name: 'chat',
      component: KidsChat,
      meta: {
        requiresChild: true,
      },
    },
    {
      path: 'notifications',
      name: 'kids-notifications',
      component: KidsNotifications,
      meta: {
        requiresChild: true,
      },
    },
    {
      path: 'calendar',
      name: 'kids-calendar',
      component: KidsCalendar,
      meta: {
        requiresChild: true,
      },
    },
    {
      path: 'qr',
      name: 'kid-qr',
      component: QR,
      meta: {
        requiresChild: true,
      },
    },
    {
      path: 'kinder/',
      component: Kindergarten,
      meta: {
        requiresChild: true,
      },
      children: [
        {
          path: 'documents',
          name: 'kinderFiles',
          component: Files,
          meta: {
            requiresChild: true,
          },
        },
        {
          path: 'object',
          name: 'kid-object',
          component: Object,
          meta: {
            requiresChild: true,
          },
        },
        {
          path: 'account',
          name: 'kinderAccount',
          component: KinderAccount,
          meta: {
            requiresChild: true,
          },
        },
        {
          path: 'account/debt/:id',
          name: 'debt',
          component: Debt,
          meta: {
            requiresChild: true,
          },
        },
      ],
    },
  ],
};
